﻿Имя автора: Andreas Rößler
http://www.math.uni-luebeck.de/mitarbeiter/roessler/publikationen.php
http://www.imada.sdu.dk/~debrabant/publik_en.php

Kristian Debrabant and Andreas Rößler: On the acceleration of the multi-level Monte Carlo method,
To appear in: J. Appl. Probab., Vol. 52, No. 2, (2015). (arXiv)

Kristian Debrabant and Andreas Rößler: Derivative-free weak approximation methods for stochastic differential equations in finance,
Recent Developments in Computational Finance - Foundations, algorithms and applications, Vol. 14, Interdisciplinary Mathematical Sciences, pp. 299-315, World Scientific (2013). (link)

Dominique Küpper, Anne Kværnø and Andreas Rößler: A Runge-Kutta method for index 1 stochastic differential-algebraic equations with scalar noise,
BIT Numerical Mathematics, Vol. 52, No. 2, 437-455 (2012). (link)

Andreas Rößler: Runge-Kutta methods for the strong approximation of solutions of stochastic differential equations,
SIAM J. Numer. Anal., Vol. 48, No. 3, 922-952 (2010). (link)

Evelyn Buckwar, Andreas Rößler and Renate Winkler: Stochastic Runge-Kutta methods for Ito-SODEs with small noise,
SIAM J. Sci. Comput., Vol. 32, No. 4, 1789-1808 (2010). (link)

Andreas Rößler: Strong and weak approximation methods for stochastic differential equations-some recent developments,
Recent Developments in Applied Probability and Statistics, p. 127-153, Physica-Verlag/Springer (2010). (link) (Preprint)

Andreas Rößler: Stochastic Taylor expansions for functionals of diffusion processes,
Stoch. Anal. Appl., Vol. 28, No. 3, 415-429 (2010). (link) (arXiv)

Andreas Rößler, Mohammed Seaïd and Mostafa Zahri: Numerical simulation of stochastic replicator models in catalyzed RNA-like polymers,
Math. Comput. Simulation, Vol. 79, No. 12, 3577-3586 (2009). (link)

Andreas Rößler: Second order Runge-Kutta methods for Itô stochastic differential equations,
SIAM J. Numer. Anal., Vol. 47, No. 3, 1713-1738 (2009). (link)

Kristian Debrabant and Andreas Rößler: Diagonally drift-implicit Runge-Kutta methods of weak order one and two for Itô SDEs and stability analysis,
Appl. Numer. Math., Vol. 59, No. 3-4, 595-607 (2009). (link)

A. Neuenkirch, I. Nourdin, A. Rößler and S. Tindel: Trees and asymptotic expansions for fractional stochastic differential equations,
Ann. Inst. Henri Poincaré Probab. Stat., Vol. 45, No. 1, 157-174 (2009). (link)

Kristian Debrabant and Andreas Rößler: Families of efficient second order Runge-Kutta methods for the weak approximation of Itô stochastic differential equations,
Appl. Numer. Math., Vol. 59, No. 3-4, 582-594 (2009). (link)

Andreas Rößler, Mohammed Seaïd and Mostafa Zahri: Method of lines for stochastic boundary-value problems with additive noise,
Appl. Math. Comput., Vol. 199, No. 1, 301-314 (2008). (link)

Kristian Debrabant and Andreas Rößler: Continuous Runge-Kutta methods for Stratonovich stochastic differential equations,
Monte Carlo and Quasi-Monte Carlo Methods 2006, pp. 237-250, Springer-Verlag (2008). (link)

Kristian Debrabant and Andreas Rößler: Classification of stochastic Runge-Kutta methods for the weak approximation of stochastic differential equations,
Math. Comput. Simulation, Vol. 77, No. 4, 408-420 (2008). (link)

Kristian Debrabant and Andreas Rößler: Continuous weak approximation for stochastic differential equations,
J. Comput. Appl. Math., Vol. 214, No. 1, 259-273 (2008). (link)

Andreas Rößler: Second order Runge-Kutta methods for Stratonovich stochastic differential equations,
BIT Numerical Mathematics, Vol. 47, No. 3, 657-680 (2007). (link)

Dominique Küpper, Jürgen Lehn and Andreas Rößler: A step size control algorithm for the weak approximation of stochastic differential equations,
Numer. Algorithms, Vol. 44, No. 4, 335-346 (2007). (link)

Peter E. Kloeden and Andreas Rößler: Runge-Kutta methods for affinely controlled nonlinear systems,
J. Comput. Appl. Math., 205 (2), 957-968 (2007). (link)

Andreas Rößler: Runge-Kutta methods for Itô stochastic differential equations with scalar noise,
BIT Numerical Mathematics, Vol. 46, No. 1, 97-110 (2006). (link)

Andreas Rößler: Rooted tree analysis for order conditions of stochastic Runge-Kutta methods for the weak approximation of stochastic differential equations,
Stochastic Anal. Appl., Vol. 24, No. 1, 97-134 (2006). (link) (arXiv)

Andreas Rößler: Explicit order 1.5 schemes for the strong approximation of Itô stochastic differential equations,
Proc. Appl. Math. Mech., 5 (1), 817-818 (2005). (link)

Andreas Rößler: Stochastic Taylor expansions for the expectation of functionals of diffusion processes,
Stochastic Anal. Appl., Vol. 22, No. 6, 1553-1576 (2004). (link) (arXiv)

Andreas Rößler: An adaptive discretization algorithm for the weak approximation of stochastic differential equations,
Proc. Appl. Math. Mech., 4 (1), 19-22 (2004). (link)

Andreas Rößler: Runge-Kutta methods for Stratonovich stochastic differential equation systems with commutative noise,
J. Comput. Appl. Math., 164-165, 613-627 (2004). (link)

Andreas Rößler: Coefficients of Runge-Kutta schemes for Itô stochastic differential equations,
Proc. Appl. Math. Mech., 3 (1), 571-572 (2003). (link)

Andreas Rößler: Embedded stochastic Runge-Kutta methods,
Proc. Appl. Math. Mech., 2 (1), 461-462 (2003). (link)

E. Kropat, A. Rössler, St. W. Pickl, G.-W. Weber: On theoretical and practical relations between discrete optimization and nonlinear optimization,
Computational Technologies (Vychisl. Tekhnol.), 7, Spec. Iss., 27-62 (2002). (link) (link)

J. Lehn, A. Rößler and O. Schein: Adaptive schemes for the numerical solution of SDEs - a comparison,
J. Comput. Appl. Math., 138 (2), 297-308 (2002). (link)

E. Kropat, St. Pickl, A. Rössler, G.-W. Weber: A new algorithm from semi-infinite optimization for a problem of time-minimum control,
Computational Technologies (Vychisl. Tekhnol.), 5, No.4, 67-81 (2000). (link) (link) 