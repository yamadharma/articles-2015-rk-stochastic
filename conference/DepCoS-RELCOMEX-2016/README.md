# 11th International Conference on Dependability and Complex Systems DepCoS-RELCOMEX 2016 #

<http://depcos.pwr.wroc.pl/index.html>

## Deadlines ##

- Full version of the paper: January 24, 2016
- Notification of acceptance: February 14, 2016
- Conference registration: February 28, 2016
- Camera-ready papers: March 6, 2016


## Main topics ##

### General problems ###

- General aspects of systems, networks theory and communication technology
- Dependability theory of networks, systems and its elements
- Artificial Intelligence
- Theory of information and IT systems
- System safety
- Business management systems

### Methodology and tools ###

- System modelling for threats, dependability and maintenance
- Evaluation of software, hardware and networks dependability
- Risk analysis, security and quality assurance
- Functional system safety - analyse, creation and evaluation
- Efficiency analysis of contemporary systems and networks
- Neural networks, fuzzy logic, genetic algorithms, expert systems for complex systems analysis and modelling
- Soft computing for complex systems analysis and modelling
- Dependability analysis in business management systems

### Technologies ###

- Security of networks, systems, software and hardware
- Cryptography methods and tools
- Coding and dependability of data transmission
- Fault tolerant computing
- Maintenance and operational planning
- AI technologies for dependability
- Internet technologies
- Functional and economic aspects of complex systems and business management systems

### Applications ###

- Planning, maintenance and SLA policies in contemporary systems and networks
- Internet dependability and quality of service
- Software dependability, testing, validation and verification
- Industrial automation systems
- Transport systems: scheduling, routing and maintenance organization
- e-economic, e-commerce, e-business, e-management
- e-learning
- e-health
