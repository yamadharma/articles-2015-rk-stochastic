# Changes #

1. The layout is fixed based on Springer template.
2. The numbering of formulas is fixed.
3. The example of implementation is based now on the active queue management algorithm (RED).