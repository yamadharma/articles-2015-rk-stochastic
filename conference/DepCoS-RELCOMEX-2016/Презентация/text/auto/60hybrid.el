(TeX-add-style-hook
 "60hybrid"
 (lambda ()
   (LaTeX-add-labels
    "sec:hybrid"
    "sec:modelica"
    "sec:tcp_mod_hybrid"
    "fig:tcp_general")))

