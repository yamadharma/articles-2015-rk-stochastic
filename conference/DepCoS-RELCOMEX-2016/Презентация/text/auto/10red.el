(TeX-add-style-hook
 "10red"
 (lambda ()
   (LaTeX-add-labels
    "sec:red"
    "fig:red-modul"
    "fig:red_p"
    "eq:red_p"
    "sec:model"
    "fig:model:dumbbell"
    "sec:tcp_model"
    "sec:tcp_congestion"
    "fig:tcp_stad")))

