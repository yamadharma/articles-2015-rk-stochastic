(TeX-add-style-hook
 "cas_tensor"
 (lambda ()
   (LaTeX-add-labels
    "eq:9"
    "eq:11"
    "eq:12"
    "eq:gamma:sym:1"
    "eq:gamma:sym:2"
    "eq:gamma:sym:3"
    "eq:gamma:sym:4")))

