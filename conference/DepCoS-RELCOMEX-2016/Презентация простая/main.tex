\input{preambule} % Включаем преамбулу

\logo{\includegraphics[height=1.0cm]{img/logo.png}} %%Эмблема университета.
\title[Stochastic Runge–Kutta]{Stochastic Runge–Kutta Software Package for Stochastic Differential Equations}

\author[Migran N. Gevorkyan]{%
	Migran N. Gevorkyan \inst{1} \and 
	Tatiana R. Velieva\inst{1} \and 
	Anna V. Korolkova\inst{1} \and 
	Leonid A. Sevastianov\inst{1,2} \and 
	Dmitry S. Kulyabov\inst{1,3}
}

\institute[PFUR]{
	\inst{1}Peoples' Friendship University of Russia \and
	\inst{2}Bogoliubov Laboratory of Theoretical Physics, Joint Institute for Nuclear Research \and
	\inst{2}Laboratory of Information Technologies, Joint Institute for Nuclear Research
}

\date[DepCoS-RELCOMEX 2016]{
    11\textsuperscript{th} International Conference on, \\
		Dependability and Complex Systems
    June 27\textsuperscript{th}--July 1\textsuperscript{st}, 2016}

\begin{document}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%				 Титульная страница/Содержание
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}
	\titlepage
\end{frame}
%%-----------------------------------------------------------------------------------------
\section*{Content}
\begin{frame}[shrink]
    \frametitle{Content}
    \tableofcontents % Делаем содержание
\end{frame}
%%-----------------------------------------------------------------------------------------
% \begin{frame}
	% \frametitle{The goals of the report}
	% \begin{itemize}
		% \item Review of numerical methods for stochastic differential equations integration.
		% \item Discussion of difficulties in realisation of these methods.
		% \item Some applications examples.
	% \end{itemize}
% \end{frame}
%%-----------------------------------------------------------------------------------------
%            Введение
%%-----------------------------------------------------------------------------------------
\section{Introduction}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Ordinary differential equation (ODE)}
	$\mathbf{x}(t) = (x^{1}(t), \ldots, x^{d}(t))^{T}$ --- smooth vector-function of one argument:
	\[
		\dfrac{\d \mathbf{x}(t)}{\d t} = \mathbf{f}(t, \mathbf{x}(t)),
	\]
	Numerical methods for of ordinary differential equation are very well developed and there are many robust software packages for numerical integration of different types of ODEs.
\end{frame}
%%-----------------------------------------------------------------------------------------
\section{Stochastic differential equation}
%%-----------------------------------------------------------------------------------------
\subsection{Definitions}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Itô stochastic differential equation with multidimensional Wiener process}
	$\mathbf{x}(t) = (x^{1}(t), \ldots, x^{d}(t))^{T}$ --- stochastic process, \textbf{s}tochastic \textbf{d}ifferential \textbf{e}quation (SDE) in \alert{Itô form}:
	\[
		\mathbf{x}(t) = \mathbf{f}(t, \mathbf{x}(t))\d t + \mathrm{G}(t, \mathbf{x}(t))\d \mathbf{W},
	\]
	\begin{itemize}
		\item $\mathbf{f}(t,\mathbf{x}(t))$ --- \alert{a drift vector},
		\item $G(t, \mathbf{x}(t))$ --- \alert{a diffusion matrix},
		\item $\mathbf{W} = (W^{1},\dots,W^{m})^T$ --- multidimensional Wiener process, which is called \alert{driving} process of SDE.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Stochastic Wiener process}
  Stochastic process $W = \{W_t, 0 \leqslant t \leqslant T\}$ is called scalar \alert{Wiener process}. This process is used to represent implicit stochastic properties of modeled system.
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{img/Winer_process}
		\caption{Some Wiener process trajectory.}
		\label{fig:wiener}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%            Область применения СДУ и проблемы их численного решения
%%-----------------------------------------------------------------------------------------
\subsection{Applications}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Application of stochastic differential equations}
Stochastic differential equations are widely used in different fields of mathematical modelling.
	\begin{itemize}
		\item Financial mathematics, stock market modelling.
		\item Population dynamics.
		\item Chemical kinetics.
		\item Physics (Diffusion process, Langevin dynamics).
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
%%    Проблемы численного решения СДУ
%%-----------------------------------------------------------------------------------------
\section{Numerical solutions}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Problems of solution of stochastic differential equations}
		\begin{itemize}
		\item Only a small number of stochastic differential equations have known analytical solutions.
		\item Classical numerical methods are not suitable for the integration of stochastic differential equations.
		\item Numerical methods for stochastic differential equations are much less developed.
		\item Existing stochastic numerical schemes are much more cumbersome compared to the classical ones.
		\item Existing libraries have implementations of only simple methods with low order of accuracy.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{Types of convergence}
%%-----------------------------------------------------------------------------------------
\begin{frame}
  \frametitle{Two types of convergence $\Rightarrow$ two types of numerical methods}
	There are two approaches to the definition of \emph{convergence}.
	\setbeamercolor{block body}{bg=gray!5,fg=black}
  \setbeamercolor{block title}{bg=white!90!black,fg=black}
	\begin{block}{Strong convergence}
		First type of convergence is \alert{strong convergence}. Strong convergence is needed when one wish to approximate a specific solution for same Wiener process.
	\end{block}
	\begin{block}{Weak convergence}
		The convergence in \alert{weak sense} one needs in case of approximation of first, second, etc. momenta and functionals from stochastic variable.
	\end{block}
	Thus, different numerical methods are needed for strong and weak approximation. We denote stochastic approximation order as $p_{s}$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{Runge-Kutta methods}
%%-----------------------------------------------------------------------------------------
\begin{frame}
  \frametitle{Available implemented methods}
	Most libraries for the numerical solution of stochastic differential equations that are freely available on the Internet implement Euler-Maruyama's method, Milstein  method and Taylor-series based methods.
	\begin{itemize}
		\item Euler-Maruyama's method is the simplest method, generalisation of classical Euler method.
		\item Milstein and Taylor-series methods require derivatives of drift vector and diffusion matrix.
	\end{itemize}
\end{frame}
%%------------------------------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Stochastic Runge-Kutta methods}
	A. Rößler in his work \textit{Strong and Weak Approximation Methods for Stochastic Differential Equations}  has developed stochastic generalisation of classical Runge-Kutta methods. Let's note some of their features
	\begin{itemize}
		\item Much more cumbersome compared to the classical ones.
		\item The number of coefficients depends on the order of the method.
		\item The stochastic order can be fractional, because method step is under the root sign.
		\item Greatest order of weak method equals $2.0$ and for strong method $1.5$
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
%%   Таблица реализованных численных методов
%%-----------------------------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{List of implemented stochastic numerical methods in our Python library}
	\begin{itemize}
		\item Euler-Maruyama method  for systems of SDE (strong $p_s = 0.5$ and weak $p_s = 1.0$)
		\item Runge–Kutta with strong convergence for systems of SDE ($p_s = 1.5$)
		\item Runge–Kutta with strong convergence for scalar SDE ($p_s = 1.0$)
		\item Runge-Kutta with weak convergence for systems of SDE ($p_s = 2.0$)
	\end{itemize}
\end{frame}

%%-----------------------------------------------------------------------------------------
%          Программная реализация и примеры использования
%%-----------------------------------------------------------------------------------------
\section{Features of the implementation}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Software realisation features I/III.}
	\begin{itemize}
		\item Functions for manipulation with multidimensional arrays (up to 4 dimensions) are needed.
		\item It is critical to be able of parallel execution of code parts to use Monte-Carlo method with large amount of tests;
		\item Generation of large amount of random variables.
		\item We use only open-source software.
	\end{itemize}
	For the implementation we use \texttt{Python} with \texttt{NumPy} and \texttt{SciPy} libs.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Software realisation features II/III.}
	\begin{itemize}
		\item When choosing a specific method we must take into account what type of convergence is necessary to provide for this specific task, as well as which of the stochastic equations must be solved --- in Ito / Stratonovich form. This increases the number of algorithms that must be programmed.
		\item For methods with strong order $p_{s} > 1$ on each step we have to approximate double stochastic integrals.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Software realisation features II/III.}
	\begin{itemize}
		\item We need to summarize the four-dimensional arrays. Summation using nested loops significantly reduce performance. We used the \texttt{numpy} functions \texttt{einsum} and \texttt{tensor\_dot}.
		\item To test weak methods the Monte Carlo method should be used. Series of $10^7$--$10^8$ tests are needed.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\section{List of examples}
%%-----------------------------------------------------------------------------------------
%%              Примеры использования
%%-----------------------------------------------------------------------------------------
%% Логорифмическое блуждание
%%---------------------------
\subsection{Logarithmic walk}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{"Logarithmic walk"}
		The scalar SDE with known solution:
		\[\mathrm{d}x(t) = \mu x(t) \mathrm{d}t + \sigma x(t) \mathrm{d}W,\;\; x(0) = x_{0},\]
		solution:
		\[x(t) = x_0 \exp{\left((\mu - 0.5\sigma^2)t + \sigma W(t)\right)},\]
		let $\mu = 2,\; \sigma = 1$ и $ x_{0} = 1$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Using stochastic RK to logarithmic walk equation}}
	\begin{figure}
		\centering
		\includegraphics[width=0.85\linewidth]{img/logarithmic_walk}
		\caption{Approximation error of strong Runge--Kutta methods for \textbf{scalar} SDE}
		\label{fig:log}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Ornstein–Uhlenbeck process
%%---------------------------
\subsection{Ornstein–Uhlenbeck process}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Ornstein–Uhlenbeck process}
		the Ornstein–Uhlenbeck process (Leonard Ornstein, George Eugene Uhlenbeck) --- stochastic process that describes the velocity of a massive Brownian particle under the influence of friction
	\begin{equation}
			\mathrm{d}x(t) = \beta(x(t)-\alpha)\mathrm{d}t + \sigma \mathrm{d}W(t),\;\; x(0) = x_{0}.
	\end{equation}
	solution:
	\begin{equation}
			x(t) = \alpha + (x_0-\alpha)\exp(-\beta t) + \dfrac{\sigma}{\sqrt{2\beta}}\sqrt{1-\exp(-2\beta t)}\mathrm{d} W
	\end{equation}
	Let $\mu = 2,\; \sigma = 1$, а начальное значени $ x_{0} = 1$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Using stochastic RK to Ornstein–Uhlenbeck process}}
	\begin{figure}
		\centering
		\includegraphics[width=0.85\linewidth]{img/OrnsteinUhlenbeck_process}
		\label{fig:OrnsteinUhlenbeck}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%% stochastic oscillator
%%---------------------------
\subsection{Stochastic oscillator}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Stochastic oscillator}
	An example of 2d SDE equation
	\begin{equation}
			\left\{
			\begin{aligned}
					& \mathrm{d} x = (-\lambda x - \omega y) \mathrm{d}t + \sigma \mathrm{d}W^{x},\\
					& \mathrm{d} y = (\omega  x - \lambda y) \mathrm{d}t + \sigma \mathrm{d}W^{y}
			\end{aligned}
			\right.
	\end{equation}
	\begin{equation}
			f(x,y) = 
			\begin{bmatrix}
					-\lambda x - \omega y\\
					\omega x - \lambda y
			\end{bmatrix},\qquad
			G(x,y) = 
			\begin{bmatrix}
					\sigma & 0\\
					0 & \sigma 
			\end{bmatrix}.
	\end{equation}
	let $\lambda = 1$, $\omega = 5$, $\sigma = 0.2$, $\mathbf{x} = (x, y) = (1, 1)^{T}$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Using stochastic RK to Stochastic oscillator}
	\begin{figure}
		\centering
		\includegraphics[width=0.85\linewidth]{img/Stochastic_oscillator}
		\label{fig:OrnsteinUhlenbeck}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Модель Блека-Шоулза
%%--------------------
\subsection{Black-Scholes model}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Black-Scholes model}
	\begin{columns}
		\begin{column}{0.2\textwidth}
			\begin{figure}
			\centering
			\includegraphics[width=0.9\linewidth]{img/Myron_Scholes.png}
			\label{fig:Myron_Scholes}
			\caption{Myron Scholes}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				\item The Black–Scholes model is a mathematical model of a financial market containing derivative investment instruments.
				\item Merton and Scholes received the 1997 Nobel Memorial Prize in Economic Sciences.
				\item Black–Scholes equation can be expressed as SDE equation.
			\end{itemize}
		\end{column}
		\begin{column}{0.2\textwidth}
			\begin{figure}
			\centering
			\includegraphics[width=0.9\linewidth]{img/Fischer_Black.jpg}
			\label{fig:strong1}
			\caption{Fischer Black}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}
\begin{frame}
	\frametitle{A two-dimensional Black-Scholes model}
	Itô SDE:
	\[
	\mathbf{x}(t) = 
	\begin{pmatrix}
		a_{1}x^{1}\\
		a_{2}x^{2}
	\end{pmatrix}
	\mathrm{d}t + 
	\begin{bmatrix}
		b_1 x^{1}&0\\
		\rho b_2 x^{2} &b_2 \sqrt{1-\rho^2} x^{2}
	\end{bmatrix}
	\begin{pmatrix}
		\mathrm{d}W^{1}\\
		\mathrm{d}W^{2}
	\end{pmatrix}
	\]
	The solution:
	\begin{align*}
		&x^{1}(t) = x^{1}_{0}\exp\left((a_{1} - 0.5b^{2}_{1})t + b_{1}W^{1}(t)\right)\\
		&x^{2}(t) = x^{2}_{0}\exp\left((a_{2} - 0.5b^{2}_{2})t + b_{2}\left(\rho W^{1}(t) + \sqrt{1+\rho^2}W^{2}(t)\right)\right),
	\end{align*}
	$x^{1}_{0} = x^{1}_{0} = 1.0$, $a_{1} = a_{2} = 0.1$, $b_{1} = b_{2} = 0.2$, $\rho = 0.8$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Exact solution for two-dimensional Black-Scholes model}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{img/BlackScholes01}
		\label{fig:strong1}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Numerical solution for Black-Scholes model (strong Runge--Kutta methods)}}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{img/BlackScholes02}
		\label{fig:strong1}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Comparison of numerical and analytical solutions for Black-Scholes model}}
	\begin{figure}
		\centering
		\includegraphics[width=0.8\linewidth]{img/BlackScholes03}
		\label{fig:strong1}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Заклбчение
%%-----------------------------------------------------------------------------------------
\section{Conclusion}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Conclusion}
	\begin{itemize}
		\item Stochastic Runge--Kutta methods are reviewed.
		\item These methods are implemented in \texttt{python 3} language with use of \texttt{numpy} and \texttt{scipy} libs.
		\item A number of difficulties of program implementation of these methods and ways to overcome them are presented.
		\item Software implementation is verified by numerical integration of a number of SDE with the analytical solution.
	\end{itemize}
	
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Литература
%%-----------------------------------------------------------------------------------------
\begin{frame}[shrink]
	\frametitle{References}
	\begin{thebibliography}{10}    
	\beamertemplatebookbibitems
	\bibitem{KP}
		Kloeden~P.~E.,~Platen~E. {\em Numerical Solution of Stochastic Differential Equations}. Berlin Heidelberg New-York: Springer, 1995.
	\beamertemplatearticlebibitems
	\bibitem{RA2010}
		Rößler~A. Strong and Weak Approximation Methods for Stochastic Differential Equations. {\em preprint}, 2010.
	\beamertemplatearticlebibitems
	\bibitem{W2001}
		Wiktorsson~M. Joint characteristic function and simultaneous simulation of iterated Itô integrals for multiple independent Brownian motions. {\em  The Annals of Applied Probability}. –– 2001. –– Vol. 11, no. 2. –– P. 470–487.
	\bibitem{RA2003}
		Rößler~A. Runge-Kutta Methods for the Numerical Solution of Stochastic Differential Equations. {\em preprint}, 2003.
	\end{thebibliography}
\end{frame}

\clearpage
\end{document}