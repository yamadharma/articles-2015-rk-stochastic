\input{preambule} % Включаем преамбулу

\logo{\includegraphics[height=1.0cm]{img/logo.png}} %%Эмблема университета.
\title[Stochastic Runge–Kutta]{Stochastic Runge–Kutta Software Package for Stochastic Differential Equations}

\author[Migran N. Gevorkyan]{%
	Migran N. Gevorkyan \inst{1} \and 
	Tatiana R. Velieva\inst{1} \and 
	Anna V. Korolkova\inst{1} \and 
	Leonid A. Sevastianov\inst{1,2} \and 
	Dmitry S. Kulyabov\inst{1,3}
}

\institute[PFUR]{
	\inst{1}Peoples' Friendship University of Russia \and
	\inst{2}Bogoliubov Laboratory of Theoretical Physics, Joint Institute for Nuclear Research \and
	\inst{2}Laboratory of Information Technologies, Joint Institute for Nuclear Research
}

\date[DepCoS-RELCOMEX 2016]{
    11\textsuperscript{th} International Conference on, \\
		Dependability and Complex Systems
    June 27\textsuperscript{th}--July 1\textsuperscript{st}, 2016}

\begin{document}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%				 Титульная страница/Содержание
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}
	\titlepage
\end{frame}
%%-----------------------------------------------------------------------------------------
\section*{Content}
\begin{frame}[shrink]
    \frametitle{Content}
    \tableofcontents % Делаем содержание
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{The goals of the report}
	
	\begin{itemize}
		\item Review of stochastic Runge-Kutta methods
		\item Discussion of difficulties in realisation of these methods
		\item Some applications examples.
	\end{itemize}
	
\end{frame}
%%-----------------------------------------------------------------------------------------
%            Вводные сведения о СДУ
%%-----------------------------------------------------------------------------------------
\section{Stochastic differential equations (SDE}
\subsection{Stochastic Wiener process}
\begin{frame}
	\frametitle{Stochastic Wiener process}
  Stochastic process $W = \{W_t, 0 \leqslant t \leqslant T\}$ is called scalar \alert{Wiener process}, if the following conditions are met:
	\begin{enumerate}
		\item $P\{W_0 = 0\} = 1$, i.e. $W_0 = 0$ almost surely.
		\item $W_t$ --- process with independent increments, i.e. $\{\Delta W_{0}, \Delta W_{1}, \ldots\}$ independent random variables; $\Delta W_{t_i} = W_{t_{i+1}} - W_{t_{i}}$ и $0 \leqslant t_{0} < t_{1} < t_{2} < \ldots  < t_{n} \leqslant T$.
		\item $\Delta W_{i} = W_{t_{i+1}} - W_{t_{i}} \sim \mathcal{N}(0,t_{i+1}-t_{i})$ where $0\leqslant t_{i+1} < t_{i} < T$, $i=0,1,\ldots,n-1$.
	\end{enumerate}
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{A Wiener process trajectory example}
\begin{frame}
	\frametitle{A Wiener process trajectory example}
	\begin{figure}
		\centering
		\includegraphics[width=1.0\linewidth]{img/Winer_process}
		\caption{Some Wiener process trajectory.}
		\label{fig:wiener}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{Itô stochastic differential equation with multidimensional Wiener process}
\begin{frame}
	\frametitle{Itô stochastic differential equation with multidimensional Wiener process}
	$\mathbf{x}(t) = (x^{1}(t), \ldots, x^{d}(t))^{T}\in\mathrm{L}^{2}(\Omega)$ --- stochastic process, SDE in \alert{Itô form}:
	\[
		x^{\alpha}(t) = f^{\alpha}(t,x^{\gamma}(t))\d t + \sum\limits_{\beta=1}^{m}G^{\alpha}_{\beta}(t,x^{\gamma}(t))\d W^{\beta},\;\alpha,\gamma = 1,\ldots,d,\;\beta = 1,\ldots,m
	\]
	\begin{itemize}
		\item $f^{\alpha}(t,x^{\gamma}(t)) = f^{\alpha}(t,x^{1}(t),\ldots,x^{d}(t))$ --- \alert{a drift vector},
		\item $g^{\alpha}_{\beta}(t,x^{\gamma}(t))$ --- \alert{a diffusion matrix},
		\item $W^{\alpha} = (W^{1},\dots,W^{m})^T$ --- multidimensional Wiener process, which is colled \alert{driving} process of SDE.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Types of convergence of the approximating function}
	\begin{definition}
		Sequence of approximating functions
		$\{x^{\alpha}_{n}\}^{\alpha=1,\ldots,d}_{n=1,\ldots,N}$ converges in the \alert{strong sense} with order $p$ to solution $x^{\alpha}(t)$ of SDE in time $T$, if there is a constant $C>0$ and some $\delta_{0}>0$, such that $\forall h \in (0,\delta_{0}]$ the following condition are met
			\[\mathbb{E}(\|x^{\alpha}(T) - x^{\alpha}_{N}\|) \leqslant Ch^{p}.\]
	\end{definition}
	\begin{definition}
		Sequence of approximating functions
		$\{x^{\alpha}_{n}\}^{\alpha=1,\ldots,d}_{n=1,\ldots,N}$ converges in the \alert{weak sense} with order $p$ to solution $x^{\alpha}(t)$ of SDE in time $T$, if there is a constant $C_{F}>0$ and some $\delta_{0}>0$, such that $\forall h \in (0,\delta_{0}]$ the following condition are met
		\[ \left|\mathbb{E}\left[F(\mathbf{x}(T))\right] - \mathbb{E}\left[F(\mathbf{x}_{N})\right]\right| \leqslant C_{F}h^{p}.\]
	\end{definition}
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{Multiple Itô integrals}
\begin{frame}
	\frametitle{Multiple Itô integrals}
	Numerical schemes require calculation of the following Itô integrals:
	\[
	I^{i}(h_{n}) = \int\limits_{t_{n}}^{t_{n+1}} \mathrm{\d}W^{i}(\tau),\quad
	I^{ij}(h_{n}) = \int\limits_{t_{n}}^{t_{n+1}}\int\limits_{t_{n}}^{\tau_{1}}\mathrm{d}W^{i}(\tau_{2})\mathrm{d}W^{j}(\tau_{1}),
	\]
 $i,j=1\ldots,m$ и $W^{i}$ --- components of a multidimensional Wiener process. Exact formulas:
	\[I^{i}(h_{n}) = \Delta W^{i}_{n},\]
	\[I^{ii}(h_{n}) = \dfrac{1}{2}\left( (\Delta W^{i}_{n})^2 - \Delta t_{n}\right),\]
	for all other cases $i\neq j$ approximation are required.
\end{frame}
%%-----------------------------------------------------------------------------------------
%           Численные методы решения СДУ
%%-----------------------------------------------------------------------------------------
\section{Numerical methods for solving ODE}
\subsection{Classification of numerical methods (Butcher's cube)}
\begin{frame}
	\frametitle{Classification of numerical methods (Butcher's cube)}
	\begin{figure}
		\centering
		\includegraphics[width=0.5\linewidth]{img/butcher_cub}
		\caption{Куб Бутчера}
		\label{fig:butcher}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
\subsection{Euler-Maruyama method}
\begin{frame}
	\frametitle{Euler-Maruyama method}
	The generalization of the classical Euler method for the case of the SDE:
	\begin{align*}
		&x^{\alpha}_{0} = x^{\alpha}(t_{0}),\\
		&x^{\alpha}_{n+1} = x^{\alpha}_{n} + f^{\alpha}(t_{n},	x^{\alpha}_{n})h_{n} + \sum\limits^{d}_{\beta=1}G^{\alpha}_{\beta}(t_{n},x^{\gamma}_{n})\Delta W^{\beta}_{n}.
	\end{align*}
	The method has a strong convergence order $p_{s} = 0.5$ and weak convergence order $p_{w} = 1.0$
\end{frame}
%%-----------------------------------------------------------------------------------------
%           Сильные методы Рунге--Кутты  для скалярного СДУ
%%-----------------------------------------------------------------------------------------
\subsection{Strong Runge--Kutta methods for scalar SDE}
\begin{frame}
	\frametitle{Stochastic Runge--Kutta with strong convergence}
	Method with \alert{strong} order $p_{s}=1.5$ for \alert{scalar} SDE:
	\begin{align*}
	&X^{i}_{0} = x_{n} + \sum\limits^{s}_{j=1}A_{0j}^{i}f(t_{n} + c^{j}_{0}h_{n}, X^{j}_{0})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{0j}g(t_{n} + c^{j}_{1}h_{n}, X^{j}_{1})\dfrac{I^{10}(h_{n})}{\sqrt{h_{n}}},\\
	&X^{i}_{1} = x_{n} + \sum\limits^{s}_{j=1}A_{1j}^{i}f(t_{n} + c^{j}_{0}h_{n}, X^{j}_{0})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{1j}g(t_{n} + c^{j}_{1}h_{n}, X^{j}_{1})\sqrt{h_{n}},\\
	&x_{n+1} = x_{n} + \sum\limits^{s}_{i=1}a_{i}f(t_{n}+c^{i}h_{n}, X^{i}_{0})h_{n} + \\
	& \qquad + \sum\limits^{s}_{i=1}\left(b^{1}_{i}I^{1}(h_{n}) + b^{2}_{i}\dfrac{I^{11}(h_{n})}{\sqrt{h_{n}}} + b^{3}_{i}\dfrac{I^{10}(h_{n})}{h_{n}} + b^{4}_{i}\dfrac{I^{111}(h_{n})}{h_{n}}\right)g(t_{n} + c^{i}_{1},X^{i}_{1}),
	\end{align*}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Butcher tableau of the methods with strong order $p_{s}=1.5$ for scalar SDE}}
	\renewcommand{\arraystretch}{1.5} 
	\begin{columns}[c]
		\column{.5\textwidth}
		Method with strong order $(p_{d},p_{s}) = (2.0, 1.5)$
			{\renewcommand{\arraystretch}{1.1}
				\[\begin{array}{c|cccc|cccc|cccc}
					0&0&0&0&0&0&0&0&0&&&\\
					\frac34&\frac34&0&0&0&\frac{3}{2}&0&0&0&&&\\
					0&0&0&0&0&0&0&0&0&&&\\
					0&0&0&0&0&0&0&0&0&&&\\ \hline
					0&0&0&0&0&0&0&0&0&&&\\
					\frac14&\frac14&0&0&0&\frac12&0&0&0&&&\\
					1&1&0&0&0&-1&0&0&0&&&\\
					\frac14&0&0&\frac14&0&-5&3&\frac12&0&&&\\ \hline
					&\frac13&\frac23&0&0&-1&\frac43&\frac23&0&-1&\frac43&-\frac13&0\\ \hline
					&&&&&2&-\frac43&-\frac23&0&-2&\frac53&-\frac23&1
				\end{array}\]}
		\column{.5\textwidth}
		View of the generalized Butcher tableau:
			\[\begin{array}{c|c|c|c}
			c^{i}_{0} & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
			c^{i}_{1} & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
								& a_{i} & b^{1}_{i} & b^{2}_{i}\\ \hline
								&  & b^{3}_{i} & b^{4}_{i}\\
			\end{array}\]
	\end{columns}
\end{frame}
%%-----------------------------------------------------------------------------------------
%           Сильные методы Рунге--Кутты  для системы СДУ
%%-----------------------------------------------------------------------------------------
\subsection{Strong Runge--Kutta methods for system of SDE}
\begin{frame}[shrink]
	\frametitle{Stochastic methods of Runge-Kutta with strong convergence}
	A method with \alert{strong} order $p_{s}=1.0$ for \alert{system} of SDE:
	\begin{align*}
		&X^{0i\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{0j}f^{\alpha}(t_{n} + c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits_{l=1}^{m}\sum\limits^{s}_{j=1}B^{i}_{0j}G^{\alpha}_{l}(t_{n} + c^{j}_{1}h_{n},X^{lj\beta})I^{l}(h_{n}),\\
		&X^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{1j}f^{\alpha}(t_{n} + c^{j}_{0}h_{n},X^{0j\beta})h_{n} + \sum\limits_{l=1}^{m}\sum\limits^{s}_{j=1}B^{i}_{1j}G^{\alpha}_{l}(t_{n} + c^{j}_{1}h_{n},X^{lj\beta})\dfrac{I^{lk}(h_{n})}{\sqrt{h_{n}}},\\
		&x^{\alpha}_{n+1} = x^{\alpha}_{n} + \sum\limits^{s}_{i=1}a_{i}f^{\alpha}(t_{n} + c^{i}_{0}h_{n},X^{0i\beta})h_{n} + \\
		&\qquad\qquad\qquad\qquad\qquad+\sum\limits^{m}_{k=1}\sum\limits^{s}_{i=1}(b^1_{i}I^{k}(h_{n}) + b^2_{i}\sqrt{h_{n}})G^{\alpha}_{k}(t_{n} + c^{i}_{1}h_{n},X^{ki\beta}),\\
		&\alpha,\beta = 1,\ldots,d,\;i,j=1,\ldots,s,\;l=1,\ldots,m.
	\end{align*}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Butcher tableau of the methods with strong order $p_{s}=1.0$ for system of SDE}}
	\renewcommand{\arraystretch}{1.5} 
	\begin{columns}[c]
		\column{.5\textwidth}
		Method with strong order $(p_{d},p_{s}) = (1.0, 1.0)$
			\[{\renewcommand{\arraystretch}{1.1}%
			\begin{array}{c|ccc|ccc|ccc}
				0&0&0&0&0&0&0&&&\\
				0&0&0&0&0&0&0&&&\\
				0&0&0&0&0&0&0&&&\\ \hline
				0&0&0&0&0&0&0&&&\\
				0&0&0&0&1&0&0&&&\\
				0&0&0&0&-1&0&0&&&\\ \hline
				&1&0&0&1&0&0&0&\frac{1}{2}&\frac{1}{2}
			\end{array}}\]
		\column{.5\textwidth}
		View of the generalized Butcher tableau:
		\[ {\renewcommand{\arraystretch}{1.7}%
		\begin{array}{c|c|c|c}
		c^{i}_0 & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
		c^{i}_1 & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
						& a_{i} & b^{1}_{i} & b^{2}_{i} \\
		\end{array}}\]
	\end{columns}
\end{frame}
%%-----------------------------------------------------------------------------------------
%           Слабые методы Рунге--Кутты  для скалярного СДУ
%%-----------------------------------------------------------------------------------------
\subsection{Weak stochastic Runge--Kutta methods  for systems of SDE}
\begin{frame}[shrink]
	\frametitle{Weak stochastic Runge--Kutta methods  for systems of SDE}
	Method with \alert{weak} order $p_{s}=2.0$  for \alert{systems} of SDE:
	\begin{align*}
& X^{0i\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A_{0j}^{i}f^{\alpha}(t_{n} + c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}\sum\limits^{m}_{l=1}B_{0j}^{i}G^{\alpha}_{l}(t_{n}+c^{j}_{1}h_{n}, X^{lj\beta})\hat{I}^{l},\\
& X^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{1j}f^{\alpha}(t_{n}+c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{1j}G^{\alpha}_{k}(t_{n} + c_{1}^{j}h_{n}, X^{kj\beta})\sqrt{h_{n}},\\
& \widehat{X}^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{2j}f^{\alpha}(t_{n}+c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}\sum\limits^{m}_{l=1,l\neq k}B^{i}_{2j}G^{\alpha}_{l}(t_{n} + c_{1}^{j}h_{n}, X^{lj\beta})\frac{\hat{I}^{kl}}{\sqrt{h_{n}}},\\
& x^{\alpha}_{n+1} = x^{\alpha}_{n} + \sum\limits^{s}_{i=1}a_{i}f^{\alpha}(t_{n}+c^{i}_{1}, X^{ki\beta})h_{n} + \sum\limits^{s}_{i=1}\sum\limits^{m}_{k=1}\left(b^{1}_{i}\hat{I}^{k} + b^{2}_{i}\frac{\hat{I}^{kk}}{\sqrt{h_{n}}}\right) G^{\alpha}_{k}(t_{n}+c^{i}_{1}h_{n}, X^{ki\beta}) + \\
& +  \sum\limits^{s}_{i=1}\sum\limits^{m}_{k=1}\left(b^{3}_{i}\hat{I}^{k} + b^{4}_{i}\sqrt{h_{n}}\right)G^{\alpha}_{k}(t_{n}+c^{i}_{2}h_{n}, \widehat{X}^{ki\beta}).
\end{align*}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Butcher tableau of the methods with weak order $p_{s}=2.0$ for systems of SDE}}
	\renewcommand{\arraystretch}{1.5} 
	\begin{columns}[c]
		\column{.5\textwidth}
		Method with weak order $(p_{d},p_{s}) = (3.0, 2.0)$
			\small{
			\[{\renewcommand{\arraystretch}{1.2}%
			\begin{array}{c|ccc|ccc|ccc}
				0&0&0&0&0&0&0&&&\\
				1&1&0&0&\frac13&0&0&&&\\
				\frac{5}{12}&\frac{25}{144}&\frac{35}{144}&0&-\frac56&0&0&&&\\ \hline
				0&0&0&0&0&0&0&&&\\
				\frac14&\frac14&0&0&\frac12&0&0&&&\\
				\frac14&\frac14&0&0&-\frac12&0&0&&&\\ \hline
				0&0&0&0&0&0&0&&&\\
				0&0&0&0&1&0&0&&&\\
				0&0&0&0&-1&0&0&&&\\ \hline
				&\frac{1}{10}&\frac{3}{14}&\frac{24}{35}&1&-1&-1&0&1&-1\\ \hline
				&&&&\frac12&-\frac14&-\frac14&0&\frac12&-\frac12\\
			\end{array}}\]}
		\column{.5\textwidth}
		View of the generalized Butcher tableau:
		\[{\renewcommand{\arraystretch}{1.5}%
			\begin{array}{c|c|c|c}
			c^{i}_{0} & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
			c^{i}_{1} & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
			c^{i}_{2} & A^{i}_{2j} & B^{i}_{2j} &\\ \hline
								& a_{i} & b^{1}_{i} & b^{2}_{i}\\ \hline
								&  & b^{3}_{i} & b^{4}_{i}\\
			\end{array}}\]
	\end{columns}
\end{frame}
%%-----------------------------------------------------------------------------------------
%          Программная реализация и примеры использования
%%-----------------------------------------------------------------------------------------
\section{Application}
\subsection{Application}
\begin{frame}
	\frametitle{Software realisation features I/III.}
	\begin{itemize}
		\item Multidimensional arrays (up to 4 dimensions) are needed.
		\item It is critical to be able of parallel execution of code parts to use Monte-Carlo method with large amount of tests;
		\item Generation of large amount of random variables.
		\item We use only open-source software.
	\end{itemize}
	For the implementation we use \texttt{Python} with \texttt{NumPy} and \texttt{SciPy} libs.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Software realisation features II/III.}
	\begin{itemize}
		\item When choosing a specific method we must take into account what type of convergence is necessary to provide for this specific task, as well as which of the stochastic equations must be solved --- in Ito / Stratonovich form. This increases the number of algorithms that must be programmed.
		\item For methods with strong order $p_{s} > 1$ on each step we have to approximate double stochastic integrals.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Software realisation features II/III.}
	\begin{itemize}
		\item We need to summarize the four-dimensional arrays. Summation using nested loops significantly reduce performance. We used the \texttt{numpy} functions \texttt{einsum} and \texttt{tensor\_dot}.
		\item To test weak methods the Monte Carlo method should be used. Series of $10^7$--$10^8$ tests are needed.
	\end{itemize}
\end{frame}
%%-----------------------------------------------------------------------------------------
%%              Примеры использования
%%-----------------------------------------------------------------------------------------
\subsection{Examples of application and tests of errors}

\begin{frame}
	\frametitle{"Logarithmic walk"}
		The scalar SDE with known solution:
		\[\mathrm{d}x(t) = \mu x(t) \mathrm{d}t + \sigma x(t) \mathrm{d}W,\;\; x(0) = x_{0},\]
		solution:
		\[x(t) = x_0 \exp{\left((\mu - 0.5\sigma^2)t + \sigma W(t)\right)},\]
		let $\mu = 2,\; \sigma = 1$ и $ x_{0} = 1$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Approximation error of strong Runge--Kutta methods for \textbf{scalar} SDE}}
	\begin{figure}
		\centering
		\includegraphics[width=1.0\linewidth]{img/SRKp1W1}
		\caption{Approximation error of strong Runge--Kutta methods for \textbf{scalar} SDE}
		\label{fig:strong1}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{A two-dimensional Black-Scholes model}
	Itô SDE:
	\[
	\mathbf{x}(t) = 
	\begin{pmatrix}
		a_{1}x^{1}\\
		a_{2}x^{2}
	\end{pmatrix}
	\mathrm{d}t + 
	\begin{bmatrix}
		b_1 x^{1}&0\\
		\rho b_2 x^{2} &b_2 \sqrt{1-\rho^2} x^{2}
	\end{bmatrix}
	\begin{pmatrix}
		\mathrm{d}W^{1}\\
		\mathrm{d}W^{2}
	\end{pmatrix}
	\]
	The solution:
	\begin{align*}
		&x^{1}(t) = x^{1}_{0}\exp\left((a_{1} - 0.5b^{2}_{1})t + b_{1}W^{1}(t)\right)\\
		&x^{2}(t) = x^{2}_{0}\exp\left((a_{2} - 0.5b^{2}_{2})t + b_{2}\left(\rho W^{1}(t) + \sqrt{1+\rho^2}W^{2}(t)\right)\right),
	\end{align*}
	$x^{1}_{0} = x^{1}_{0} = 1.0$, $a_{1} = a_{2} = 0.1$, $b_{1} = b_{2} = 0.2$, $\rho = 0.8$.
\end{frame}
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{\normalsize{Approximation error of strong Runge--Kutta methods for  SDE \textbf{system}}}
	\begin{figure}
		\centering
		\includegraphics[width=1.0\linewidth]{img/SRKp1Wm}
		\caption{Approximation error of strong Runge--Kutta methods for  SDE \textbf{system}}
		\label{fig:strong1}
	\end{figure}
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Заклбчение
%%-----------------------------------------------------------------------------------------
\begin{frame}
	\frametitle{Conclusion}
	\begin{itemize}
		\item Stochastic Runge--Kutta methods are reviewed.
		\item These methods are implemented in \texttt{python 3} language with use of \texttt{numpy} and \texttt{scipy} libs.
		\item A number of difficulties of program implementation of these methods and ways to overcome them are presented.
		\item Software implementation is verified by numerical integration of a number of SDE with the analytical solution.
	\end{itemize}
	
\end{frame}
%%-----------------------------------------------------------------------------------------
%% Литература
%%-----------------------------------------------------------------------------------------
\begin{frame}[shrink]
	\frametitle{References}
	\begin{thebibliography}{10}    
	\beamertemplatebookbibitems
	\bibitem{KP}
		Kloeden~P.~E.,~Platen~E.
		\newblock {\em Numerical Solution of Stochastic Differential Equations}.
		\newblock Berlin Heidelberg New-York: Springer, 1995.
	\beamertemplatearticlebibitems
	\bibitem{RA2010}
		Rößler~A.
		\newblock Strong and Weak Approximation Methods for Stochastic Differential Equations.
		\newblock {\em preprint}, 2010.
	\beamertemplatearticlebibitems
	\bibitem{W2001}
		Wiktorsson~M.
		\newblock Joint characteristic function and simultaneous simulation of iterated Itô integrals for multiple independent Brownian motions.
		\newblock {\em  The Annals of Applied Probability}. –– 2001. –– Vol. 11, no. 2. –– P. 470–487.
	\bibitem{RA2003}
		Rößler~A.
		\newblock  Runge-Kutta Methods for the Numerical Solution of Stochastic Differential Equations.
		\newblock {\em preprint}, 2003.
	\end{thebibliography}
\end{frame}

\clearpage
\end{document}