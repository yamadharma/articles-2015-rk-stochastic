\section{Основные понятия теории стохастических дифференциальных уравнений}

\subsection{Случайный процесс Винера}

\begin{thedefinition*}
	Случайный процесс $W(t)$, $t \geqslant 0$ называется скалярным \emph{процессом Винера} если выполняются следующие условия:
	\begin{itemize}
		\item $\mathrm{P}\{W(0)=0\} = 1$, иначе говоря $W(0) = 0$ почти наверное;
		\item $W(t)$ --- процесс с независимыми приращениями, то есть $\{\Delta W_{i}\}^{N-1}_{0}$ независимые случайные величины; $\Delta W_{i} = W(t_{i+1}) - W(t_{i})$ и $0 \leqslant t_{0} < t_{1} < t_{2} < \ldots  < t_{N} \leqslant T$;
		\item $\Delta W_{i} = W(t_{i+1}) - W(t_{i}) \sim \mathcal{N}(0,t_{i+1}-t_{i})$ где $0\leqslant t_{i+1} < t_{i} < T$, $i=0,1,\ldots,N-1$
	\end{itemize}
\end{thedefinition*}
Обозначение $\Delta W_{i} \sim \mathcal{N}(0,\Delta t_{i})$ говорит о том, что $\Delta W_{i}$ --- нормально распределенная случайная величина с математическим ожиданием $\mathbb{E}[\Delta W_{i}] = \mu = 0$ и дисперсией $\mathbb{D}[\Delta W_{i}] = \sigma^{2} = \Delta t_{i}$.

Винеровский процесс является моделью \emph{броуновского движения} (хаотического блуждания). Если рассмотреть процесс $W(t)$ в те моменты времени $0 = t_{0} < t_{1} < t_{2} < \ldots < t_{N-1} < t_{N}$ когда он испытывает случайные аддитивные изменения, то непосредственно из определения следует:
\[
		W(t_{1}) = W(t_{0}) + \Delta W_0,\;\; W(t_{2}) = W(t_{1}) + \Delta W_1,\ldots,W(t_{N}) = W(t_{N-1}) + \Delta W_{N-1},
\]
где по $\Delta W_{i} \sim \mathcal{N}(0,\Delta t_{i})$, $\forall i = 0,\ldots,N-1$.

Запишем $W(t_{N})$ в виде накопленной суммы:
\[
	W(t_{N}) = W(t_{0}) + \sum\limits_{i=0}^{N-1}\Delta W_{i} 
\]
и учтем, что $\mathbb{E}[\Delta W_{i}] = 0$ и $\mathbb{D}[\Delta W_{i}] = \Delta t_{i}$ можно показать, что сумма нормально распределенных случайных чисел $\Delta W_{i}$ также является нормально распределенным случайным числом:
\[
	\mathbb{E}\sum\limits_{i=0}^{N-1}\Delta W_{i} = 0, \;\; \mathbb{D}\sum\limits_{i=0}^{N-1}\Delta W_{i} = \sum\limits_{i=0}^{N-1} \Delta t_{i} = T,\;\; \sum\limits_{i=0}^{N-1}\Delta W_{i} \sim \mathcal{N}(0,T).
\]

Многомерный винеровский процесс $\mathbf{W}(t)\colon \Omega\times[t_{0},T]\to \mathbb{R}^{m}$ определяют как случайный процесс составленный из совместно независимых одномерных винеровских процессов $W^{1}(t),\ldots,W^{m}(t)$. Приращения $\Delta W^{\alpha}_{i},\;\forall \alpha = 1,\ldots,m$ являются совместно независимыми нормально распределенными случайными величинами. С другой стороны вектор $\Delta W^{\alpha}_{i}$ можно представить как многомерную нормально распределенную случайную величину с вектором математических ожиданий $\mu = 1$ и диагональной матрицей ковариаций.

\subsection{Генерирование выборочных траекторий процесса Винера на компьютере}

Для генерирования выборочной траектории одномерного винеровского процесса на отрезке $[0,T]$ разбитом на $N$ частей с шагом $\Delta t_{i} = h = \mathrm{const}$ необходимо сгенерировать $N$ нормально распределенных случайных чисел $\varepsilon_{0}, \ldots, \varepsilon_{N-1}  \sim \mathcal{N}(0,h)$ и построить их накопленные суммы $\varepsilon_{0}$, $\varepsilon_{0} + \varepsilon_{1}$, $\varepsilon_{0} + \varepsilon_{1} + \varepsilon_{2}$ и так далее. В результате получим два массива длины $N$, первый \texttt{W} --- точки выборочной траектории и второй \texttt{dW} --- приращения этой траектории. Пример траектории ~см.~рис.~\ref{fig:wiener}.
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{img/Winer_process}
	\caption{Траектория винеровского процесса}
	\label{fig:wiener}
\end{figure}

В случае $m$--мерного случайного процесса следует сгенерировать уже $m$ последовательностей из $N$ нормально распределенных случайных величин.

В описываемой нами библиотеке для генерирования выборочных винеровских траекторий написана функция:
\begin{pygmented}{python}
  (dt, t, dW, W) = wiener_process(N, dim=1, interval=(0.0, 1.0))
\end{pygmented}
Обязательны аргумент $N$ --- число точек разбиения временного интервала \texttt{interval} (по умолчанию $[0,1]$), а \texttt{dim} --- размерность винеровского процесса. Функция возвращает кортеж из четырех элементов, где \texttt{dt} --- шаг разбиения ($\Delta t_{i} = h = \mathrm{const}$), \texttt{t} --- одномерный \texttt{numpy} массив содержащий значения моментов времени $t_{1},t_{2},\ldots,t_{N}$, \texttt{dW}, \texttt{W} --- также \texttt{numpy} массивы размерностью $N\times m$ содержащие приращения $\Delta W^{\alpha}_{i}$ и точки выборочной траектории $W^{\alpha}_{i}$, где $i=1,\ldots,N;\;\alpha=1,\ldots,m$.
%%==============================================================================================================
%%===============================================================================================================

\subsection{СДУ в форме Ито для многомерного процесса Винера}
Введем вероятностное пространство $(\Omega, \mathscr{A}, \mathbb{P})$, где $\Omega$ --- пространство элементарных событий, $\mathscr{A}$ --- сигма-алгебра подмножеств пространства $\Omega$, $\mathbb{P}$ --- вероятностная мера. На отрезке $[t_{0}, T] \in \setR^{1}$ определена переменная $t$ имеющая физический смысл времени.

Рассмотрим случайный процесс $\mathbf{x}(t) = (x^{1}(t), \ldots, x^{d}(t))^{T}$, где $\mathbf{x}(t)$ принадлежит функциональному пространству $\mathrm{L}^{2}(\Omega)$ с нормой $\| \cdot \|$. Будем считать, что случайный процесс $\mathbf{x}(t)$ является решением для стохастического дифференциального уравнения (СДУ) в форме \emph{Ито}~\cite{L_Oksendal_en, L_Oksendal_ru, L_Kloeden_Platen}:
\[
	\mathbf{x}(t) = \mathbf{f}(t,\mathbf{x}(t))\d t + \mathbf{G}(t,\mathbf{x}(t))\d \mathbf{W},
\]
где $\mathbf{W} = (W^{1},\dots,W^{m})^T$ --- многомерный винеровский процесс, называемый \emph{ведущим (driver)} процессом СДУ.
Функция $\mathbf{f}\colon[t_{0}, T]\times\setR^{d} \to \setR^{d}$ называется \emph{вектором сноса}, а 
матричнозначная функция $\mathbf{G}\colon[t_{0}, T]\times\setR^{d}\times\setR^{m} \to \setR^{d}\times\setR^{m}$ --- \emph{матрицей диффузии}. Кроме того $\mathbf{f}(t,\mathbf{x}(t)) = (f^{1}(t,\mathbf{x}),\ldots,f^{d}(t,\mathbf{x}))^{T}$, а матрица $\mathbf{G}$ имеет вид:
\[
	\mathbf{G} = 
	\begin{bmatrix}
		g^{1}_{1}(t,\mathbf{x}) & g^{1}_{2}(t,\mathbf{x}) & \ldots & g^{1}_{m}(t,\mathbf{x})\\
		g^{2}_{1}(t,\mathbf{x}) & g^{2}_{2}(t,\mathbf{x}) & \ldots & g^{2}_{m}(t,\mathbf{x})\\
		\vdots & \vdots & \ddots & \vdots \\
		g^{d}_{1}(t,\mathbf{x}) & g^{d}_{2}(t,\mathbf{x}) & \ldots & g^{d}_{m}(t,\mathbf{x})
	\end{bmatrix}
\]
То же уравнение можно переписать в индексном виде:
\[
	x^{\alpha}(t) = f^{\alpha}(t,x^{\gamma}(t))\d t + \sum\limits_{\beta = 1}^{m}g^{\alpha}_{\beta}(t,x^{\gamma}(t))\d W^{\beta},
\]
где $\alpha,\gamma = 1,\ldots,d$, $\beta = 1,\ldots,m$, а $f^{\alpha}(t,x^{\gamma}(t)) = f^{\alpha}(t,x^{1}(t),\ldots,x^{d}(t))$.

На отрезке $[t_{0}, T]$ введем сетку $t_{0}<t_{1}<\ldots<t_{N}=T$ с шагом $h_{n} = t_{n+1} - t_{n}$, где $n = 0,\ldots,N-1$ и максимальный шаг сетки $h  = \max{\{h_{n-1}\}^{N}_{1}}$. Далее будем полагать, что сетка равномерная, то $h_{n} = h = \const$. $\mathbf{x}_{n}$ --- сеточная функция, аппроксимирующая случайный процесс $\mathbf{x}(t)$, так что $\mathbf{x}_{0} = \mathbf{x}(t_{0})$, $\mathbf{x}_{n} \approx \mathbf{x}(t_{n})\;\forall n = 1,\ldots,N$.

%%=========================================================================================
\subsection{Слабая и сильная сходимости аппроксимирующей функции}
Необходимо определить критерий точности аппроксимации процесса $\mathbf{x}(t)$ последовательностью функций $\{\mathbf{x}_{n}\}^{N}_{1}$. Таких критериев принято выделять два: \emph{слабый} и \emph{сильный}~\cite{L_Oksendal_en, L_Andreas_2003, L_Rossler_2010}.

\begin{thedefinition}
	Последовательность аппроксимирующих функций $\{\mathbf{x}_{n}\}^{N}_{1}$ сходится в \emph{сильном смысле} с порядком $p$ к решению $\mathbf{x}(t)$ СДУ в момент времени $T$ если существует константа $C>0$ и $\delta_{0}>0$ такая что $\forall h \in (0,\delta_{0}]$ выполняется условие
	\[
		\mathbb{E}(\|\mathbf{x}(T) - \mathbf{x}_{N}\|) \leqslant Ch^{p}.
	\]
\end{thedefinition}

\begin{thedefinition}
	Последовательность аппроксимирующих функций $\{\mathbf{x}_{n}\}^{N}_{1}$ сходится в \emph{слабом смысле} с порядком $p$ к решению $\mathbf{x}(t)$ СДУ в момент времени $T$ если существует константа $C_{F}>0$ и $\delta_{0}>0$ такая что $\forall h \in (0,\delta_{0}]$ выполняется условие
	\[
		\left|\mathbb{E}\left[F(\mathbf{x}(T))\right] - \mathbb{E}\left[F(\mathbf{x}_{N})\right]\right| \leqslant C_{F}h^{p}.
	\]
\end{thedefinition}
Здесь $F \in C^{2(p+1)}_{\mathrm{P}}(\setR,\setR^{d})$ --- непрерывный дифференцируемый (вплоть до производных $2(p+1)$ порядка) функционал с полиномиальным ростом.

Если матрица $\mathbf{G}$ обращается в ноль, то условие сильной сходимости равносильно условию сходимости для детерминированного случая. В отличие от детерминированного случая порядок сильной сходимости не обязательно является натуральным числом и может принимать дробно-рациональные значения.

Выбор типа сходимости целиком зависит от решаемой задачи. Численные методы с сильной сходимостью хорошо аппроксимируют конкретные траектории случайного процесса $\mathrm{x}(t)$ и, поэтому, нуждаются в информации о траектории ведущего винеровскго процесса. На практике это означает, что функции, реализующей стохастический метод с сильной сходимостью необходимо передать двумерный массив со значениями приращений $\Delta W^{\alpha}_{n}$, где $\alpha=1,\ldots,m;\;n=1,\ldots,N$. Этот массив будет использоваться для аппроксимации кратных стохастических интегралов, которые входят в любую численную схему порядка $p > \frac12$.

В свою очередь слабые численные методы хорошо аппроксимируют характеристики вероятностного распределения случайного процесса $\mathrm{x}(t)$. Они не нуждаются в информации о траектории соответствующего винеровского процесса и случайные величины для этих методов могут быть сгенерированы на другом вероятностном пространстве, которое легко реализовать программно.
%%===============================================================================================================
\subsection{Вычисление и аппроксимация кратных интегралов Ито}
В общем случае для конструирования численных схем, порядок сильной сходимости которых был бы больше чем $p=\frac12$, необходимо включение в формулы этих схем однократных и двукратных интегралов Ито:
\[
	I^{i}(t_{n}, t_{n+1}) = I^{i}(h_{n}) = \int\limits_{t_{n}}^{t_{n+1}} \mathrm{\d}W^{i}(\tau),\;\; I^{ij}(t_{n}, t_{n+1}) = I^{ij}(h_{n}) = \int\limits_{t_{n}}^{t_{n+1}}\int\limits_{t_{n}}^{\tau_{1}}\mathrm{d}W^{i}(\tau_{2})\mathrm{d}W^{j}(\tau_{1})
\]
где $i,j=1\ldots,m$ и $W^{i}$ --- компоненты многомерного винеровского процесса.

Возникает задача выражения этих интегралов через приращения $\Delta W^{i}_{n} = W^{i}(t_{n+1}) - W^{i}(t_{n})$. В случае однократного интеграла это можно сделать для любого индекса $i$: $I^{i}(h_{n}) = \Delta W^{i}_{n}$. В случае же двукратного интеграла $I^{ij}(h_{n})$ точная формула имеет место лишь при $i=j$:
\[
	I^{ii}(h_{n}) = \dfrac{1}{2}\left( (\Delta W^{i}_{n})^2 - \Delta t_{n} \right),
\]
в остальных же случаях при $i\neq j$ выразить $I^{ij}(h_{n})$ через приращения $\Delta W^{\alpha}_{n}$ и $\Delta t_{n}$ в конечном виде не представляется возможным, поэтому остается лишь использовать численную аппроксимацию.

% В книге~\cite{L_Kloeden_Platen} приведены следующие формулы для аппроксимации двукратного интеграла Ито $I^{ij}$:
% \begin{gather*}
	% I^{ij}(h_{n}) = \dfrac{\Delta W^{i}_{n} \Delta W^{j}_{n} - h_{n}\delta^{ij}}{2} + A^{ij}(h_{n}),\\
	% A^{ij}(h_{n}) = \dfrac{h}{2\pi}\sum\limits^{\infty}_{k=1}\dfrac{1}{k}\left(V^{i}_{k}(U^{j}_{k} + \sqrt{2/h_{n}} \Delta W^{j}_{n}) - V^{j}_{k}(U^{i}_{k} + \sqrt{2/h_{n}}\Delta W^{i}_{n})\right),
% \end{gather*}
% $\Delta W^{i}_{n} \sim \mathcal{N}(0,h)$, $V^{i}_{k} \sim \mathcal{N}(0,1)$, $U^{i}_{k} \sim \mathcal{N}(0,1)$, $i=1,\ldots,m;\; k=1,\ldots,\infty$; $i=1,\ldots,N$. Из формул видно, что в случае $i=j$ получаем конечное выражение для $I^{ii}$, но в случае $i\neq j$ приходится суммировать бесконечный ряд $A^{ij}$. Данный алгоритм дает ошибку аппроксимации порядка $O(h^2/n)$, где $n$ --- число оставленных слагаемых бесконечного ряда $A^{ij}$.

В статье~\cite{L_Wiktorsson_2001} предложена матричный вид аппроксимирующих формул. Пусть $\mathbf{1}_{m\times m}$, $\mathbf{0}_{m\times m}$ --- единичная и нулевая матрицы $m\times m$, тогда
\begin{gather*}
	\mathbf{I}(h_{n}) = \dfrac{\Delta \mathbf{W}_{n} \Delta \mathbf{W}_{n} - h_{n}\mathbf{1}_{m\times m}}{2} + \mathbf{A}(h_{n}),\\
	\mathbf{A}(h_{n}) = \dfrac{h}{2\pi}\sum\limits^{\infty}_{k=1}\dfrac{1}{k}\left(\mathbf{V}_{k}(\mathbf{U}_{k} + \sqrt{2/h_{n}} \Delta \mathbf{W}_{n})^{T} - (\mathbf{U}_{k} + \sqrt{2/h_{n}}\Delta \mathbf{W}_{n})\mathbf{V}^{T}_{k}\right),
\end{gather*}
где $\Delta \mathbf{W}_{n}, \mathbf{V}_{k}, \mathbf{U}_{k}$ --- независимые нормально распределенные многомерные случайные величины:
\begin{gather*}
\Delta \mathbf{W}_{n} = (\Delta W^{1}_{n}, \Delta W^{2}_{n}, \ldots, \Delta W^{m}_{n})^{T} \sim \mathcal{N}(\mathbf{0}_{m\times m}, h_{n}\mathbf{1}_{m\times m}),\\
\mathbf{V}_{k} = (V^{1}_{k}, V^{2}_{k},\ldots, V^{m}_{k})^{T} \sim \mathcal{N}(\mathbf{0}_{m\times m}, \mathbf{1}_{m\times m}),\;\;
\mathbf{U}_{k} = (U^{1}_{k}, U^{2}_{k},\ldots, U^{m}_{k})^{T} \sim \mathcal{N}(\mathbf{0}_{m\times m}, \mathbf{1}_{m\times m}).
\end{gather*}

В нашей библиотеке написан ряд функций для аппроксимации интегралов Ито. Следующие две функции предназначены для вычисления интегралов Ито кратности 1 и 2 для одномерного процесса Винера:
\begin{pygmented}{python}
  I1 = Ito1W1(dW); I2 = Ito2W1(dW, h)
\end{pygmented}
они возвращают numpy массивы разной размерности. Функция \texttt{Ito1W1} тривиальна и возвращает массив приращений винеровского процесса, в функция \texttt{Ito2W1} возвращает numpy массив размерности \texttt{(N, 2, 2)}, что соответствует интегралам $I^{ij}(h_{n})\;\forall n = 1\ldots N,\;i,j=0,1$
Следующие две функции предназначены для вычисления аппроксимации интегралов Ито кратности 1 и 2 для многомерного процесса Винера:
\begin{pygmented}{python}
  I1m = Ito1Wm(dW, h); I2m = Ito2Wm(dW, h, n)
\end{pygmented}
они возвращают numpy массивы разной размерности. Функция \texttt{Ito1Wm} проводит вычисления на основе точных формул  и возвращает свой массив размерности \texttt{(N, m+1)}, а функция \texttt{Ito2Wm} возвращает numpy массив размерности \texttt{(N, m+1, m+1)}, что соответствует интегралам $I^{ij}(h_{n})\;\forall n = 1\ldots N,\;i,j=0,\ldots,m$. Аргумент \texttt{n} задает число членов в бесконечном ряде $A^{ij}$, который используется для аппроксимации стохастического интеграла.
%%=========================================================================================

\subsection{СДУ имеющие аналитическое решение}
Для верификации написанных программ в случае сильной сходимости будем использовать СДУ с известным аналитическим решением, что даст возможность вычислить погрешности аппроксимации реализаций траекторий ведущих винеровских процессов.
\subsubsection{Логарифмическое блуждание (одномерный винеровский процесс)}
В качестве скалярного СДУ с известным аналитическим решением возьмем уравнение \emph{логарифмического блуждания}~\cite{L_Platen_Liberati_2010, L_Kloeden_Platen}:
\[
    \mathrm{d}x(t) = \mu x(t) \mathrm{d}t + \sigma x(t) \mathrm{d}W(t),\;\; x(0) = x_{0},
\]
точное решение которого имеет вид~\cite[пункт 4.4]{L_Kloeden_Platen}
\[
    x(t) = x_0 \exp{\left((\mu - 0.5\sigma^2)t + \sigma W(t)\right)}.
\]
При вычислениях будем использовать $\mu = 2,\; \sigma = 1$ и $ x_{0} = 1$.

\subsubsection{Двумерная модель Блека--Шоулза}
Для тестирования численных методов с сильной сходимостью для систем СДУ с многомерным винеровским процессом возьмем уравнение двумерной модели Блека-Шоулза~\cite{L_Platen_Liberati_2010}, которое обычно записывается в следующей форме:
\[
    \mathbf{x}(t) = \mathrm{A}\mathbf{x}(t)\mathrm{d}t + \sum\limits^{2}_{k=1}\mathrm{B}_k \mathbf{x}(t)\mathrm{d}W^{k},
\]
где все матрицы диагональны: $A = \mathrm{diag}(a^1,a^2)$, $B_{1} = \mathrm{diag}(b^1_1,b^2_1)$ и $B_{2} = \mathrm{diag}(b^1_2,b^2_2)$. После ряда преобразования можно выразить функции $\mathbf{f}(\mathbf{x})$ и $\mathrm{G}(\mathbf{x})$ в явном виде
% в явном виде необходимо провести некоторые преобразования: 
% \begin{gather*}
    % \mathrm{A}\mathbf{x} =
    % \begin{bmatrix}
        % a^1&0\\
        % 0&a^2
    % \end{bmatrix}
    % \begin{bmatrix}
        % x^{1}(t)\\
        % x^{2}(t)
    % \end{bmatrix}
    % =
    % \begin{bmatrix}
        % a^{1}x^{1}(t)\\
        % a^{2}x^{2}(t)
    % \end{bmatrix},\\
    % \begin{bmatrix}
        % b^1_1&0\\
        % 0&b^2_1
    % \end{bmatrix}
    % \begin{bmatrix}
        % x^{1}(t)\\
        % x^{2}(t)
    % \end{bmatrix}
    % \mathrm{d}W^{1}
    % +
    % \begin{bmatrix}
        % b^1_2&0\\
        % 0&b^2_2
    % \end{bmatrix}
    % \begin{bmatrix}
        % x^{1}(t)\\
        % x^{2}(t)
    % \end{bmatrix}
    % \mathrm{d}W^{2}
    % =
    % \begin{bmatrix}
        % b^1_1 x^{1}(t)&b^1_2 x^{1}(t)\\
        % b^2_1 x^{2}(t)&b^2_2 x^{2}(t)
    % \end{bmatrix}
    % \begin{bmatrix}
        % W^{1}(t)\\
        % W^{2}(t)
    % \end{bmatrix}.
% \end{gather*}
\[
    \mathrm{f}(\mathbf{x}) = 
    \begin{bmatrix}
        a^{1}x^{1}(t)\\
        a^{2}x^{2}(t)
    \end{bmatrix},\;\;
    \mathrm{G}(\mathbf{x}) = 
    \begin{bmatrix}
        b^1_1 x^{1}(t)&b^1_2 x^{1}(t)\\
        b^2_1 x^{2}(t)&b^2_2 x^{2}(t)
    \end{bmatrix}.
\]
Аналитическое решение можно записать в виде экспоненты, которая берется поэлементно
\[
    \mathbf{x}(t) = \mathbf{x}_0 \exp \left\{\left(\mathrm{A} - \frac{1}{2}\sum\limits^{2}_{k=1}B_k^2\right)t + \sum\limits^{2}_{k=1}\mathrm{B}_k W^k (t)\right\}.
\]
Для проведения вычислений возьмем следующие матрицы:  $A = \mathrm{diag}(a_1,a_2)$, $B_{1} = \mathrm{diag}(b_1,b_2\rho)$ и $B_{2} = \mathrm{diag}(0,b_2\sqrt{1-\rho})$
%\[
    %\mathrm{A} = 
    %\begin{bmatrix}
        %a_1&0\\
        %0&a_2
    %\end{bmatrix},\;\;
    %\mathrm{B}_{1} = 
    %\begin{bmatrix}
        %b_{1}&0\\
        %0&b_{2}\rho
    %\end{bmatrix},\;\;
    %\mathrm{B}_{2} =
    %\begin{bmatrix}
        %0&0\\
        %0&b_{2}\sqrt{1-\rho^2}
    %\end{bmatrix},
%\]
и следующие значения параметров $x^{1}_{0} = x^{1}_{0} = 1.0$, $a_{1} = a_{2} = 0.1$, $b_{1} = b_{2} = 0.2$, $\rho = 0.8$. Тогда точное решение уравнения имеет вид:
\begin{align*}
    &x^{1}(t) = x^{1}_{0}\exp\left((a_{1} - 0.5b^{2}_{1})t + b_{1}W^{1}(t)\right)\\
    &x^{2}(t) = x^{2}_{0}\exp\left((a_{2} - 0.5b^{2}_{2})t + b_{2}\left(\rho W^{1}(t) + \sqrt{1+\rho^2}W^{2}(t)\right)\right)
\end{align*}