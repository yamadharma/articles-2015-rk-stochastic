\section{Стохастические методы Рунге--Кутты}
В данном разделе мы приведем несколько наиболее эффективных стохастических численных схем Рунге--Кутты, основываясь на результатах статьи~\cite{L_Rossler_2010}. Вначале укажем ряд факторов, которые делают стохастические методы Рунге-Кутта существенно сложнее классических методов.
\begin{itemize}
	\item При выборе конкретного метода надо учитывать какой тип сходимости необходимо обеспечить для данной конкретной задачи, а также какое из стохастических уравнений необходимо решать --- в форме Ито или в форме Стратоновича. Это увеличивает количество алгоритмов, которые нужно запрограммировать.
	\item Для методов с сильной сходимостью $p_{s} > 1$ На каждом шаге необходимо решать ресурсоемкую задачу по аппроксимации двукратных стохастических интегралов.
	\item В численной схеме присутствуют не только матрицы и векторы, но и тензоры (четырехмерные массивы) с которыми необходимо совершать операцию свертки по нескольким индексам. Реализация свертки через суммирование с помощью обычных циклов приводит к существенному падению производительности.
	\item Для использования слабых методов необходимо применять метод Монте Карло, проводя несколько серий по $10^7$--$10^8$ испытаний в каждой.
\end{itemize}

Наиболее существенно на скорости работы программы сказывается необходимость свертки многомерных массивов. Для ускорения этой операции при реализации численных схем была использована функция \texttt{einsum} из библиотеки \texttt{numpy} версии 1.6 и старше. В случае использования обычных последовательных циклов производительность вычислений существенно падает.
% \begin{pygmented}{python}
  % numpy.einsum(subscripts, *operands, out=None, 
    % dtype=None, order='K', casting='safe')
% \end{pygmented}

Также следует обратить внимание на большое количество нулей в обобщенных таблицах Бутчера. Реализация отдельной функции для конкретной реализации метода часто помогает получить выигрыш в производительности в несколько раз по сравнению с функцией, которая реализует универсальный алгоритм.

\subsection{Численный метод Эйлера-Маруйамы}

Простейшим численным методом для решения как скалярных уравнений, так и систем СДУ, является метод Эйлера--Маруямы, названный так в честь Гиширо Маруямы (Gisiro Maruyama), который распространил классический метод Эйлера для ОДУ на случай СДУ~\cite{L_Maruyama_1955}. Метод легко обобщается на случай многомерного винеровского процесса.
\begin{align*}
	&x^{\alpha}_{0} = x^{\alpha}(t_{0}),\\
	&x^{\alpha}_{n+1} = x^{\alpha}_{n} + f^{\alpha}(t_{n}, x^{\alpha}_{n})h_{n} + \sum\limits^{d}_{\gamma=1}G^{\alpha}_{\beta}(t_{n},x^{\gamma}_{n})\Delta W^{\beta}_{n}.
\end{align*}
Как видно из формул на каждом шаге для вычисления следующего значения аппроксимирующей функции требуется лишь соответствующее данному шагу приращение процесса $\Delta W^{\beta}_{n}$. Метод имеет сильный порядок $(p_{d},p_{s}) = (1.0, 0.5)$. Величина $p_{d}$ обозначает порядок точности детерминированной части численного метода, то есть той точности, которую будет давать численный метод при применении его к уравнению функцией $G(t,x^{\alpha}(t))\equiv 0$. Величина $p_{s}$ обозначает порядок приближения стохастической части уравнения.

%%------------------------------------------------------------------------------------------------------------------
\subsection{Стохастические методы Рунге--Кутты сильной сходимости $p=1.5$ для одномерного винеровского процесса}
В случае скалярных уравнения и винеровского процесса справедлива следующая численная схема:
\begin{align*}
	&X^{i}_{0} = x_{n} + \sum\limits^{s}_{j=1}A_{0j}^{i}f(t_{n} + c^{j}_{0}h_{n}, X^{j}_{0})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{0j}g(t_{n} + c^{j}_{1}h_{n}, X^{j}_{1})\dfrac{I^{10}(h_{n})}{\sqrt{h_{n}}},\\
	&X^{i}_{1} = x_{n} + \sum\limits^{s}_{j=1}A_{1j}^{i}f(t_{n} + c^{j}_{0}h_{n}, X^{j}_{0})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{1j}g(t_{n} + c^{j}_{1}h_{n}, X^{j}_{1})\sqrt{h_{n}},\\
	&x_{n+1} = x_{n} + \sum\limits^{s}_{i=1}a_{i}f(t_{n}+c^{i}h_{n}, X^{i}_{0})h_{n} + \\
	& \qquad + \sum\limits^{s}_{i=1}\left(b^{1}_{i}I^{1}(h_{n}) + b^{2}_{i}\dfrac{I^{11}(h_{n})}{\sqrt{h_{n}}} + b^{3}_{i}\dfrac{I^{10}(h_{n})}{h_{n}} + b^{4}_{i}\dfrac{I^{111}(h_{n})}{h_{n}}\right)g(t_{n} + c^{i}_{1},X^{i}_{1}),
\end{align*}
обобщенная таблица Бутчера который имеет вид~\cite{L_Rossler_2010}:
\[
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|c|c|c}
		c^{i}_{0} & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
		c^{i}_{1} & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
		& a_{i} & b^{1}_{i} & b^{2}_{i}\\ \hline
		&  & b^{3}_{i} & b^{4}_{i}\\
	\end{array}}
\]
В препринте Росслера~\cite{L_Rossler_2010} для данной схемы приведены две таблицы Бутчера для метода четвертой стадийности $s=4$.
	{\scriptsize%
\[
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|cccc|cccc|cccc}
	0&0&0&0&0&0&0&0&0&&&\\
	\frac34&\frac34&0&0&0&\frac{3}{2}&0&0&0&&&\\
	0&0&0&0&0&0&0&0&0&&&\\
	0&0&0&0&0&0&0&0&0&&&\\ \hline
	0&0&0&0&0&0&0&0&0&&&\\
	\frac14&\frac14&0&0&0&\frac12&0&0&0&&&\\
	1&1&0&0&0&-1&0&0&0&&&\\
	\frac14&0&0&\frac14&0&-5&3&\frac12&0&&&\\ \hline
	&\frac13&\frac23&0&0&-1&\frac43&\frac23&0&-1&\frac43&-\frac13&0\\ \hline
	&&&&&2&-\frac43&-\frac23&0&-2&\frac53&-\frac23&1
	\end{array}}\;\;
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|cccc|cccc|cccc}
	0&0&0&0&0&0&0&0&0&&&\\
	1&\frac34&0&0&0&0&0&0&0&&&\\
	\frac12&\frac14&\frac14&0&0&1&\frac12&0&0&&&\\
	0&0&0&0&0&0&0&0&0&&&\\ \hline
	0&0&0&0&0&0&0&0&0&&&\\
	\frac14&\frac14&0&0&0&-\frac12&0&0&0&&&\\
	1&1&0&0&0&1&0&0&0&&&\\
	\frac14&0&0&\frac14&0&2&-1&\frac12&0&&&\\ \hline
	&\frac16&\frac16&\frac23&0&-1&\frac43&\frac23&0&1&-\frac43&\frac13&0\\ \hline
	&&&&&2&-\frac43&-\frac23&0&-2&\frac53&-\frac23&1
	\end{array}}
\]}
Численную схему реализуемую первой таблицей обозначим как \texttt{SRK1W1}, а вторую как \texttt{SRK2W2}. Метод \texttt{SRK1W1} имеет сильный порядок $(p_{d},p_{s}) = (2.0, 1.5)$, а метод \texttt{SRK2W1} сильный порядок $(p_{d},p_{s}) = (3.0, 1.5)$. Еще один метод с сильным порядком $p_{s}=1.0$ приведен в книге~\cite{L_Kloeden_Platen} и его таблица Бутчера имеет вид:
{\scriptsize%
\[
	\text{\texttt{KlPl}: }
	{\renewcommand{\arraystretch}{1.1}%
	\begin{array}{c|cc|cc|cc}
	0&0&0&0&0&&\\
	0&0&0&0&0&&\\ \hline
	0&0&0&0&0&&\\
	0&0&0&0&0&&\\ \hline
	0&1&0&1&0&-1&1\\
	 & & &1&0&0&0\\
	\end{array}}
\]}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1.0\textwidth]{img/SRKp1W1}
	\caption{Сильные погрешности методов для скалярного процесса $W$ c $h=10^{-3}$}
	\label{fig:fig1}
\end{figure}

В описываемой библиотеке написано несколько функций для решения СДУ со скалярным винеровским процессом
\begin{pygmented}{python}
  x_num = EulerMaruyama(f, g, h, x_0, dW)
  x_num = strongSRKW1(f, g, h, x_0, dW, name='SRK2W1')
\end{pygmented}
где \texttt{f(x)} и \texttt{g(x)} --- функции, принимающие в качестве аргумента и возвращающие действительное число; \texttt{h} ---  шаг сетки, \texttt{x\_0} --- начальное значение, \texttt{dW} --- массив приращений винеровского процесса \texttt{N}; аргумент \texttt{name} может принимать значения \texttt{'SRK1W1'} и \texttt{'SRK2W2'}.

%%------------------------------------------------------------------------------------------------------------------

\subsection{Стохастические методы Рунге--Кутты сильной сходимости $p=1.0$ для многомерного винеровского потока}
Для системы СДУ Ито с многомерным винеровским процессом можно построить стохастическую численную схему Рунге-Кутты сильного порядка $p_{s} = 1.0$ с использованием однократных и двукратных интегралов Ито~\cite{L_Rossler_2010}.
\begin{align*}
	&X^{0i\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{0j}f^{\alpha}(t_{n} + c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits_{l=1}^{m}\sum\limits^{s}_{j=1}B^{i}_{0j}G^{\alpha}_{l}(t_{n} + c^{j}_{1}h_{n},X^{lj\beta})I^{l}(h_{n}),\\
	&X^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{1j}f^{\alpha}(t_{n} + c^{j}_{0}h_{n},X^{0j\beta})h_{n} + \sum\limits_{l=1}^{m}\sum\limits^{s}_{j=1}B^{i}_{1j}G^{\alpha}_{l}(t_{n} + c^{j}_{1}h_{n},X^{lj\beta})\dfrac{I^{lk}(h_{n})}{\sqrt{h_{n}}},\\
	&x^{\alpha}_{n+1} = x^{\alpha}_{n} + \sum\limits^{s}_{i=1}a_{i}f^{\alpha}(t_{n} + c^{i}_{0}h_{n},X^{0i\beta})h_{n} + \sum\limits^{m}_{k=1}\sum\limits^{s}_{i=1}(b^1_{i}I^{k}(h_{n}) + b^2_{i}\sqrt{h_{n}})G^{\alpha}_{k}(t_{n} + c^{i}_{1}h_{n},X^{ki\beta}),
\end{align*}
$n=0,1,\ldots,N-1$; $i=1,\ldots,s$; $\beta,k=1,\ldots,m$; $\alpha = 1,\ldots,d$. Обобщенная таблица Бутчера имеет вид~\cite{L_Rossler_2010}:
\[
	{\renewcommand{\arraystretch}{1.4}%
	\begin{array}{c|c|c|c}
		c^{i}_0 & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
		c^{i}_1 & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
		& a_{i} & b^{1}_{i} & b^{2}_{i} \\
	\end{array}}
\]
В препринте Росслера~\cite{L_Rossler_2010} указаны две таблицы Бутчера для метода третей стадийности $s=3$:
{\scriptsize%
\[
	\text{\texttt{SRK1Wm}: }
	{\renewcommand{\arraystretch}{1.1}%
	\begin{array}{c|ccc|ccc|ccc}
	0&0&0&0&0&0&0&&&\\
	0&0&0&0&0&0&0&&&\\
	0&0&0&0&0&0&0&&&\\ \hline
	0&0&0&0&0&0&0&&&\\
	0&0&0&0&1&0&0&&&\\
	0&0&0&0&-1&0&0&&&\\ \hline
	&1&0&0&1&0&0&0&\frac{1}{2}&\frac{1}{2}
	\end{array}}\;\;
	\text{\texttt{SRK2Wm}: }
	{\renewcommand{\arraystretch}{1.1}%
	\begin{array}{c|ccc|ccc|ccc}
	0&0&0&0&0&0&0&&&\\
	1&1&0&0&0&0&0&&&\\
	0&0&0&0&0&0&0&&&\\ \hline
	0&0&0&0&0&0&0&&&\\
	1&1&0&0&1&0&0&&&\\
	1&1&0&0&-1&0&0&&&\\ \hline
	&\frac{1}{2}&\frac{1}{2}&0&1&0&0&0&\frac{1}{2}&-\frac{1}{2}
	\end{array}}
\]}
Метод \texttt{SRK1Wm} имеет сильный порядок $(p_{d},p_{s}) = (1.0, 1.0)$, а метод \texttt{SRK2Wm} сильный порядок $(p_{d},p_{s}) = (2.0, 1.0)$.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1.0\textwidth]{img/SRKp1Wm}
	\caption{Сильные погрешности методов для двумерного процесса $W^{\alpha}$ c $h=10^{-3}$}
	\label{fig:fig2}
\end{figure}

В описываемой библиотеке для решения систем СДУ с многомерным процессом Винера (предполагается, что $d=m$) написано несколько функций
\begin{pygmented}{python}
  x_num = strongSRKp1Wm(f, G, h, x_0, dW, name='SRK1Wm')
  x_num = EulerMaruyamaWm(f, G, h, x_0, dW)
\end{pygmented}
где \texttt{f(x)} и \texttt{G(x)} --- функции, принимающие в качестве аргумента массив \texttt{x} и возвращающие numpy-массивы размерности \texttt{m} и \texttt{(m,m)}; \texttt{h} ---  шаг сетки, \texttt{x\_0} --- начальное значение (одномерный массив размерности $m$), \texttt{dW} --- массив приращений винеровского процесса \texttt{(N,m)}; аргумент \texttt{name} может принимать значения \texttt{'SRK1Wm'} и \texttt{'SRK2Wm'}.

\subsection{Стохастические методы Рунге--Кутты слабой сходимости $p=2.0$}
Численные методы со слабой сходимостью хорошо аппроксимируют характеристики распределения случайного процесса $x^{\alpha}(t)$. Слабый численный метод не нуждается в информации о траектории винеровского процесса $W^{\alpha}_{n}$ и случайные величины для этих методов могут быть сгенерированы на другом вероятностном пространстве. Поэтому можно использовать с легко моделируемом на компьютере распределением.
\begin{align*}
& X^{0i\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A_{0j}^{i}f^{\alpha}(t_{n} + c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}\sum\limits^{m}_{l=1}B_{0j}^{i}G^{\alpha}_{l}(t_{n}+c^{j}_{1}h_{n}, X^{lj\beta})\hat{I}^{l},\\
& X^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{1j}f^{\alpha}(t_{n}+c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}B^{i}_{1j}G^{\alpha}_{k}(t_{n} + c_{1}^{j}h_{n}, X^{kj\beta})\sqrt{h_{n}},\\
& \widehat{X}^{ki\alpha} = x^{\alpha}_{n} + \sum\limits^{s}_{j=1}A^{i}_{2j}f^{\alpha}(t_{n}+c^{j}_{0}h_{n}, X^{0j\beta})h_{n} + \sum\limits^{s}_{j=1}\sum\limits^{m}_{l=1,l\neq k}B^{i}_{2j}G^{\alpha}_{l}(t_{n} + c_{1}^{j}h_{n}, X^{lj\beta})\frac{\hat{I}^{kl}}{\sqrt{h_{n}}},\\
& x^{\alpha}_{n+1} = x^{\alpha}_{n} + \sum\limits^{s}_{i=1}a_{i}f^{\alpha}(t_{n}+c^{i}_{1}, X^{ki\beta})h_{n} + \sum\limits^{s}_{i=1}\sum\limits^{m}_{k=1}\left(b^{1}_{i}\hat{I}^{k} + b^{2}_{i}\frac{\hat{I}^{kk}}{\sqrt{h_{n}}}\right) G^{\alpha}_{k}(t_{n}+c^{i}_{1}h_{n}, X^{ki\beta}) + \\
& +  \sum\limits^{s}_{i=1}\sum\limits^{m}_{k=1}\left(b^{3}_{i}\hat{I}^{k} + b^{4}_{i}\sqrt{h_{n}}\right)G^{\alpha}_{k}(t_{n}+c^{i}_{2}h_{n}, \widehat{X}^{ki\beta})
\end{align*}
Обобщенная таблица Бутчера имеет вид~\cite{L_Rossler_2010}:
\[
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|c|c|c}
		c^{i}_{0} & A^{i}_{0j} & B^{i}_{0j} &\\ \hline
		c^{i}_{1} & A^{i}_{1j} & B^{i}_{1j} &\\ \hline
		c^{i}_{2} & A^{i}_{2j} & B^{i}_{2j} &\\ \hline
		& a_{i} & b^{1}_{i} & b^{2}_{i}\\ \hline
		&  & b^{3}_{i} & b^{4}_{i}\\
	\end{array}}
\]
{\scriptsize%
\[
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|ccc|ccc|ccc}
		0&0&0&0&0&0&0&&&\\
		1&1&0&0&\frac13&0&0&&&\\
		\frac{5}{12}&\frac{25}{144}&\frac{35}{144}&0&-\frac56&0&0&&&\\ \hline
		0&0&0&0&0&0&0&&&\\
		\frac14&\frac14&0&0&\frac12&0&0&&&\\
		\frac14&\frac14&0&0&-\frac12&0&0&&&\\ \hline
		0&0&0&0&0&0&0&&&\\
		0&0&0&0&1&0&0&&&\\
		0&0&0&0&-1&0&0&&&\\ \hline
		&\frac{1}{10}&\frac{3}{14}&\frac{24}{35}&1&-1&-1&0&1&-1\\ \hline
		&&&&\frac12&-\frac14&-\frac14&0&\frac12&-\frac12\\
	\end{array}}\quad
%	{\scriptsize%
	{\renewcommand{\arraystretch}{1.5}%
	\begin{array}{c|ccc|ccc|ccc}
		0&0&0&0&0&0&0&&&\\
		1&1&0&0&1&0&0&&&\\
		0&0&0&0&0&0&0&&&\\ \hline
		0&0&0&0&0&0&0&&&\\
		1&1&0&0&1&0&0&&&\\
		1&1&0&0&-1&0&0&&&\\ \hline
		0&0&0&0&0&0&0&&&\\
		0&0&0&0&1&0&0&&&\\
		0&0&0&0&-1&0&0&&&\\ \hline
		&\frac{1}{2}&\frac{1}{2}&0&\frac12&\frac14&\frac14&0&\frac12&-\frac12\\ \hline
		&&&&-\frac12&\frac14&\frac14&0&\frac12&-\frac12\\
	\end{array}}
\]}

В численной схеме слабого стахостического метода Рунге-Кутты используются следующие случайные величины:
\[
	\hat{I}^{kl} = 
	\left\{
		\begin{aligned}
			&\dfrac{1}{2}(\hat{I}^{k}\hat{I}^{l} - \sqrt{h}_{n}\tilde{I}^{k}),\; k<l,\\
			&\dfrac{1}{2}(\hat{I}^{k}\hat{I}^{l} + \sqrt{h}_{n}\tilde{I}^{l}),\; l<k,\\
			&\dfrac{1}{2}((\hat{I}^{k})^2 - h_{n}).\; k=l.
		\end{aligned}
	\right.
\]
Величина $\hat{I}^{k}$ имеет трехточечное распределение. Это означает, что $\hat{I}^{k}$ может принимать три значения $ \{-\sqrt{3h_{n}}, 0, \sqrt{3h_{n}}\}$ с вероятностями $1/6$, $2/3$ и $1/6$ соответственно. В свою очередь величина $\tilde{I}^{k}$ имеет двухточечное распределение $\{-\sqrt{h_{n}}, \sqrt{h_{n}}\}$ с вероятностями $1/2$ и $1/2$.

В описываемой библиотеке для генерирования массива случайных чисел, распределенных по $n$--точечному распределению, написана функция
\begin{pygmented}{python}
  n_point_distribution(values, probabilities, shape)
\end{pygmented}
где аргумент \texttt{values} задает список или массив из $n$ точек $x_{1},\ldots,x_{n}$, аргумент \texttt{probabilities} задает массив/список вероятностей $p_{1},\ldots,p_{n}$, а аргумент \texttt{shape} форму требуемого массива генерируемых случайных чисел. Для генерирования величин $\hat{I}^{kl}$ предназначена функция
\begin{pygmented}{python}
  (I_hat, I) = weakIto(h, dim, N)
\end{pygmented}
где \texttt{h} --- шаг сетки, \texttt{dim} --- размерность винеровского процесса и \texttt{N} --- число точек разбиения отрезка интегрирования. Возвращаемые величины $\hat{I}^{k}$ и $\hat{I}^{kl}$.
%%-=--------------------------------------------------------------------------------=
\subsection{Оценка сильной погрешности}
Для оценки сильной сходимости достаточно сгенерировать одну траекторию ведущего процесса Винера и для этой траектории рассчитать точное решение с помощью аналитической формулы и приближенное решения с помощью численных методов. После этого можно найти как локальную $\|\mathbf{x}(t_{n}) - \mathbf{x}_{n}\|$, $n=1,\ldots,N$ так и глобальную $\|\mathbf{x}(T) - \mathbf{x}_{N}\|$ ошибки аппроксимации.
% \item генерируем новую траекторию и проводим те же вычисления для нее;
% \item повторяем $M$ раз;
% \item по завершению для каждого метода получаем двухмерных массив локальной погрешности $[N,M]$;
% \item вычисляем среднее арифметическое по второй размерности и получаем $N$ чисел, каждое из которых будет сильной локальной погрешностью в каждой точке траектории. 

\subsection{Оценка слабой погрешности}
Оценка слабой сходимости значительно более трудоемкая задача, для решения которой используется метод Монте-Карло. Проводится $M$ --- испытаний, то есть к исходному СДУ $M$ раз применяется изучаемый численный метод. В результате получаем множество независимых одинаково распределенных случайных величин $\mathbf{x}_{nm}$, где $m=1,\ldots,M$ --- номер испытания методом Монте-Карло, а $n=1,\ldots,N$ номер шага метода. Так как обычно интересуются глобальной погрешностью метода, то рассматривают значения аппроксимирующей функции на конце отрезка интегрирования $[t_{0}, T]$, где $t_{N}=T$ или же в любой фиксированной промежуточной точке отрезка.

Необходимо аппроксимировать функционалы вида $\mathbb{E}[f(\mathbf{x}(T))]$ с помощью выборочного среднего (эмпирического математического ожидания) от $\{\mathbf{x}_{Nm}\}^{M}_{1}$
\[
	\bar{f}(\mathbf{x}_{N}) = \frac{1}{M}\sum\limits^{M}_{m=1}f(\mathbf{x}_{Nm})
\]

Оценка сходимости метода Монте-Карло основывается на центральной предельной теореме, которая позволяет дать оценку абсолютного значения отклонения выборочного среднего от математического ожидания величины $\mathbf{x}$.
\[
	\frac{1}{M}\sum\limits^{M}_{m=1}f(\mathbf{x}_{m}) - \mathbb{E}[f(\mathbf{x})] \xrightarrow[M\to\infty]{} \mathcal{N}\left(0, \frac{\mathbb{D}[f(\mathbf{x})]}{M}\right)
\]
или иначе
\[
	\mathbb{D}\left[\bar{f}(\mathbf{x}) - \mathbb{E}[f(\mathbf{x})]\right] = \frac{\mathbb{D}[f(\mathbf{x})]}{M}
\]
\[
	\mathbb{D}\left[\bar{f}(\mathbf{x}) - \mathbb{E}[f(x)]\right] = \mathbb{E}\left[\left(\bar{f}(\mathbf{x}) - \mathbb{E}[f(x)] - 0\right)^2\right] = \mathbb{E}\left[\bar{f}(\mathbf{x}) - \mathbb{E}[f(x)]\right]^2
\]
так как выполняется неравенство $\mathbb{D}[f(\mathbf{x})] = \mathbb{E}[f(\mathbf{x})]^{2} - (\mathbb{E}[f(\mathbf{x})])^2 \leqslant \mathbb{E}[f(\mathbf{x})]^2$, то справедлива следующая оценка среднеквадратичной сходимости метода Монте Карло:
\[
	\sqrt{\mathbb{E}\left[\bar{f}(\mathbf{x}) - \mathbb{E}[f(x)]\right]^2} \leqslant \frac{\sqrt{\mathbb{E}[f(\mathbf{x})]^2}}{\sqrt{M}}.
\]

Метод Монте-Карло, таким образом, сходится как $1/\sqrt{M}$ и, кроме того, на его сходимость влияет значение второго момента функции $f(\mathbf{x})$. Если это значение велико, то число испытаний методом Монте-Карло необходимо увеличить.

Чтобы оценить глобальную слабую погрешность вычислений необходимо учесть как погрешность метода Монте Карло, так и погрешность самой численной схемы.
\[
	\mathrm{err} = \left| \mathbb{E}[f(\mathbf{x}(T))] - \frac{1}{M}\sum\limits^{M}_{m=1}f(\mathbf{x}_{Nm})\right| \leqslant \left| \mathbb{E}[f(\mathbf{x}(T))] - \mathbb{E}[f(\mathbf{x}_{N})]\right| + \left| \mathbb{E}[f(\mathbf{x_{N}})] - \frac{1}{M}\sum\limits^{M}_{m=1}f(\mathbf{x}_{Nm})\right|
\]
Общая ошибка складывается из \emph{ошибки дискретизации} численной схемы и из \emph{статистической ошибки}. Для проверки ошибки дискретизации с помощью численных экспериментов необходимо чтобы статистическая ошибка была меньше ошибки дискретизации.

На практике это означает, что число $M$ экспериментов методом Монте-Карло должно быть велико до такой степени, чтобы обеспечить статистическую погрешность меньшую ошибки дискретизации. Для построения доверительного интервала следует провести $K$ серий экспериментов методом Монте-Карло по $N$ испытаний в каждом. Для каждой серии следует вычислить \emph{абсолютную среднюю ошибку}:
\[
	\mathrm{MAE} = |\mu| = | \bar{f}(\mathbf{x}_{N}) - \mathbb{E}[f(\mathbf{x}(T))]|
\]
получим последовательность из $K$ величин $|\mu_{k}|$, где $k=1,\ldots,K$. Далее вычисляем эмпирическую дисперсию абсолютной средней ошибки $\sigma^{2}_{\mu}$ по формуле
\[
	\sigma^{2}_{\mu} = \frac{1}{K}\sum\limits^{K}_{k=1}(\mu_{k} - \mu)^{2}
\]
где
\[
	\mu = \frac{1}{MK}\sum\limits^{K}_{k=1}\sum\limits^{M}_{m=1}f(\mathbf{x}_{Nmk}) - \mathbb{E}[f(\mathbf{x}(T))]
\]
Вычисление доверительного интервала $[\mu - \Delta \mu, \mu + \Delta \mu]$, где 
\[
\Delta \mu = t_{1-\frac{\alpha}{2},K}\sqrt{\frac{\sigma^{2}_{\mu}}{K}}
\]
$t_{1-\frac{\alpha}{2},K}$ --- $(1-\frac{1}{\alpha})$-квантиль распределения Стьюдента с $K$ степенью свободы.