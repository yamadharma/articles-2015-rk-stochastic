# -*- coding: utf-8 -*-

import multiprocessing as mp
import numpy as np
import sys
import sde

# Функции правой части СДУ
def f(x):
    """Вектор сноса"""
    return np.array([(-273.0/512.0)*x[0], -(1.0/160.0)*x[0] + (-785.0/512.0 + np.sqrt(2)/8.0)*x[1]])

def G(x):
    """Матрица диффузии"""
    return np.array([[0.25*x[0], (1.0/16.0)*x[0]],
                     [x[1]*(1.0-2.0*np.sqrt(2))/4.0, 0.1*x[0] + x[1]/16.0]])


def calculations(sim_nums, x_0, N, seed):
    """Функция, которая будет проводить вычисления
    x1_num, x2_num --- массив с координатами x1 и x2,
    где в каждой строке результат одной симуляции (N чисел)
    и всего len(sim_nums)) строк.
    """
    x1_nums = np.empty(shape=(len(sim_nums), N))
    x2_nums = np.empty(shape=(len(sim_nums), N))
    
    (dt, t, dW, W) = sde.wiener_process(N, dim=2, seed=seed)

    count = 1
    for num in sim_nums:
        print("\rstep № {0}".format(count), end="", flush=True)
        (dt, t, dW, W) = sde.wiener_process(N, dim=2, seed=seed+count)
        #x_num = sde.weakSRKp2Wm(f, G, dt, x_0, dW)
        #x_num = sde.strongSRKp1Wm1(f, G, dt, x_0, dW)
        #x_num = sde.weakRK22W2(f, G, dt, x_0, dW, seed=seed+count)
        x_num = sde.EulerMaruyamaWm(f, G, dt, x_0, dW)
        #x_num = sde.strongSRKp1Wm1(f, G, dt, x_0, dW)

        x1_nums[num, :] = x_num[:, 0]
        x2_nums[num, :] = x_num[:, 1]

        count += 1
    
    return (x1_nums, x2_nums)

if __name__ == '__main__':
    # proc_num --- число процессов
    # sim_num --- число симуляций
    proc_num = int(sys.argv[1])
    sim_num = int(sys.argv[2])

    sim_nums = [np.arange(0, sim_num/proc_num, 1) for i in range(proc_num)]
    seeds = [int(i*sim_num/proc_num) for i in range(proc_num)]
    print(seeds)
    # Параметры уравнения и винеровского процесса
    x_0 = np.array([1.0, 1.0])
    if len(sys.argv) < 4:
        N = 1.0e3
    else:
        N = float(sys.argv[3])
    
    method = "EM"

    print("Метод: {0},\nчисло процессов: {1},\nчисло симуляций: {2},\nчисло точек {3}".format(method, proc_num, sim_num, N))

    # Создаем пулл процессов и запускаем их
    pool = mp.Pool(processes=proc_num)
    results = [pool.apply_async(calculations, (nums, x_0, N, seed)) for (nums, seed) in zip(sim_nums,seeds)]

    x1_nums = np.concatenate([r.get()[0] for r in results])
    x2_nums = np.concatenate([r.get()[1] for r in results])


    # Сохраняем полученные массивы

    np.save("{0}_N{1}_sim{2}_x1.npy".format(method, int(N), int(sim_num)), x1_nums)
    np.save("{0}_N{1}_sim{2}_x2.npy".format(method, int(N), int(sim_num)), x2_nums)