# -*- coding: utf-8 -*-

import multiprocessing as mp
import numpy as np
import sys
import sde

a, k1, k2, k3 = (0.5, 3.0, 0.05, 2.5)
#a, k1, k2, k3 = (0.2, 3.0, 2.5, 10.85)

#----------------------------------------------------------------------
def matrix_sqrt(M):
    """Вычисление квадратного корня из матрицы"""
    U, s, V = np.linalg.svd(M, full_matrices=True)
    S = np.diag(s)
    return np.dot(U, np.dot(np.sqrt(S), V))

def f(x):
    """Вектор сноса"""
    return np.array([k1*a*x[0] - k2*x[0]*x[1], k2*x[0]*x[1] - k3*x[1]])

def G(x):
    """Матрица диффузии"""
    return matrix_sqrt(np.array([[x[0]*(k1*a + k2*x[1]), -k2*x[0]*x[1]],
                           [-k2*x[0]*x[1], x[1]*(k2*x[0] + k3)]]))
#----------------------------------------------------------------------

def calculations(sim_nums, x_0, N, seed):
    """Функция, которая будет проводить вычисления
    x1_num, x2_num --- массив с координатами x1 и x2,
    где в каждой строке результат одной симуляции (N чисел)
    и всего len(sim_nums)) строк.
    """
    x1_nums = np.zeros(shape=(len(sim_nums), N))
    x2_nums = np.zeros(shape=(len(sim_nums), N))
    
    (dt, t, dW, W) = sde.wiener_process(N, dim=2, interval=(t_0, T), seed=seed)
    
    count = 1
    for num in sim_nums:
        print("\rstep № {0}".format(count), end="", flush=True)
        #(dt, t, dW, W) = sde.wiener_process(N, dim=2, interval=(t_0, T), seed=seed)
        x_num = sde.weakRK22W2(f, G, dt, x_0, dW, seed=seed+count)
        #x_num = sde.strongSRKp1Wm1(f, G, dt, x_0, dW)
        
        # В случае, если или хищники или жертвы мерают полностью,
        # массивы заполняются значениями до len(x_num[:,i]) i=1,2
        x1_nums[num, :len(x_num[:,0])] = x_num[:, 0]
        x2_nums[num, :len(x_num[:,1])] = x_num[:, 1]
        count += 1
    return (x1_nums, x2_nums)

if __name__ == '__main__':
    # proc_num --- число процессов
    # sim_num --- число симуляций
    proc_num = int(sys.argv[1])
    sim_num = int(sys.argv[2])

    sim_nums = [np.arange(0, sim_num/proc_num, 1) for i in range(proc_num)]
    seeds = list(range(proc_num))
    # Параметры уравнения и винеровского процесса
    t_0 = 0.0
    T = 20.0
    
    # стационарные точки и начальное значение
    M1 = (0.0,0.0)
    M2 = (k3/k2, a*k1/k2)
    x_0 = (M2[0]+0.1, M2[1]-20.0)
    
    if len(sys.argv) < 4:
        N = 1.0e3
    else:
        N = float(sys.argv[3])
    
    method = "RK"

    print("Метод: {0},\nчисло процессов: {1},\nчисло симуляций: {2},\nчисло точек {3}".format(method, proc_num, sim_num, N))

    # Создаем пулл процессов и запускаем их
    pool = mp.Pool(processes=proc_num)
    results = [pool.apply_async(calculations, (nums, x_0, N, seed)) for (nums, seed) in zip(sim_nums,seeds)]

    x1_nums = np.concatenate([r.get()[0] for r in results])
    x2_nums = np.concatenate([r.get()[1] for r in results])


    # Сохраняем полученные массивы
    np.save("Результат {0} прогонов Жертвы.npy".format(int(sim_num)),x1_nums)
    np.save("Результат {0} прогонов Хищники.npy".format(int(sim_num)),x2_nums)